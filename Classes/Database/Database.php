<?php

namespace AppsTeam\ExamsSupervision\Database;

class Database{

    static $Apps;
    
    public static function GetApps(){

        if(Database::$Apps != null) return Database::$Apps;

        $env = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . '/env.ini.php', true);
        Database::$Apps = new \MySQLi($env['connections']['apps']['server'], $env['connections']['apps']['username'], $env['connections']['apps']['password'], $env['connections']['apps']['database']);
        Database::$Apps->set_charset('utf8');

        return Database::$Apps;
    }
}