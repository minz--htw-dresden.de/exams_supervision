<?php

namespace AppsTeam\ExamsSupervision\Data;

use AppsTeam\ExamsSupervision\Database\Database;
use AppsTeam\ExamsSupervision\Data\LockedTimeSpan;

class Employee{

    /* #region Properties */

    /**
     * @property Int ID of the Employee
     * 
     */
    protected $id;

    /**
     * @property String First Name of the Employee
     * 
     */
    protected $first_Name;

    /**
     * @property String Last Name of the Employee
     */
    protected $last_Name;

    /**
     * @property String The Employees Title(s)
     */
    protected $title;

    /**
     * @property String The Email of this Employee
     */
    protected $email;

    /**
     * @property Int The Faculty this Employee is assigned to
     */
    protected $fak;

    /**
     * @property Int The Planning Priority for this Employee (0, 1)
     */
    protected $priority;

    /**
     * @property Boolean True if the Employee is a Professor
     */
    protected $is_Prof;

    /**
     * @property Boolean True if the Employee is a Dozent
     */
    protected $is_Doz;

    /**
     * @property Boolean True if the Employee is female
     */
    protected $female;

    /**
     * @property Array<LockedTimeSpan> Array of all Locked Times for this Employee
     */
    protected $lockedTimes;

    /**
     * @property Array<Exam> Array of all Exams the Employee is already planned for
     */
    protected $plannedSupervisions;

    /**
     * @property Array<Exam> Array of all Exams held by this Employee
     */
    protected $ownExams;

    /* #endregion Properties */

    /* #region Getters */
    public function getID() : int{return $this->id;}
    public function getFakID() : ?int{ return $this->fak;}
    public function getTitle() : string{return $this->title;}
    public function getFullName() : string{return $this->last_Name.", ".$this->first_Name;}
    public function getLastName() : string{return $this->last_Name;}
    public function getFirstName() : string{return $this->first_Name;}
    public function getPriority() : int{return $this->priority;}
    public function getEmail() : string{return $this->email;}
    public function isDozent() : int{return $this->is_Doz;}
    public function isProfessor():int{return $this->is_Prof;}
    public function isFemale():int{return $this->female;}
    public function getPlannedSupervisionsCount(): int{
        if($this->plannedSupervisions == null) $this->getPlannedSupervisions(); 
        return count($this->plannedSupervisions);
    }
    /* #endregion Getters */ 

    public static function CreateAssoc($assoc) : Employee{
        $employee = new Employee();
        $employee->id         = $assoc["m_pers_id"];
        $employee->first_Name = $assoc["vorname"];
        $employee->last_Name  = $assoc["name"];
        $employee->title      = $assoc["titel"];
        $employee->email      = $assoc["email"];
        $employee->fak        = $assoc["fk_m_orgeinheit_id"];
        $employee->is_Prof    = $assoc["ist_prof"] != null ? $assoc["ist_prof"] : 0;
        $employee->is_Doz     = $assoc["ist_dozent"];
        $employee->female  = $assoc["ist_weiblich"];
        
        $employee->GetEmployeeInfo();

        return $employee;
    }

    public static function GetEmployeeByID($id) : ?Employee{
        $Apps = Database::GetApps();

        $sql = "SELECT * FROM m_pers WHERE m_pers_id = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $id);
        $statement->execute();
        $result = $statement->get_result();
        $row = $result->fetch_assoc();

        return $row != null ? Employee::CreateAssoc($row) : null;
    }

    /**
     * Gets all Locked Times for this Employee
     * @return Array Array of LockedTimeSpan
     */
    public function GetLockedTimes() : ?array{
        $Apps = Database::GetApps();

        $this->lockedTimes = array();
        $sql = "SELECT * FROM exams_supervision_pers_locked WHERE m_pers_id = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->id);
        $statement->execute();
        $result = $statement->get_result();
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            array_push($this->lockedTimes, LockedTimeSpan::CreateAssoc($row));
        }

        return $this->lockedTimes;
    }

    /**
     * Gets all Exams, the Employee is already planned for
     * @return Array Array of Exam
     */
    public function GetPlannedSupervisions() : ?array{
        $Apps = Database::GetApps();
        $this->plannedSupervisions = array();
        $sql = "SELECT * FROM `exams_supervision_supervisor` WHERE `m_pers_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->id);
        $statement->execute();
        $result = $statement->get_result();
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            array_push($this->plannedSupervisions, Exam::GetExamById($row["exams_planner_pruefung_id"]));
        }
        return $this->plannedSupervisions;
    }

    public function GetOwnExams(): ?array{
        $Apps = Database::GetApps();
        $this->ownExams = array();
        $sql = "SELECT `fk_exams_planner_pruefung_id` FROM `exams_planner_pruefung_pruefer` WHERE `fk_exams_planner_pruefer_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->id);
        $statement->execute();
        $result = $statement->get_result();
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            $exam = Exam::GetExamById($row["fk_exams_planner_pruefung_id"]);
            if($exam->isValid())
                array_push($this->ownExams, $exam);
        }
        return $this->ownExams;
    }

    /**
     * Gets all Additional Information about an Employee
     */
    public function GetEmployeeInfo(){
        $Apps = Database::GetApps();

        $sql = "SELECT * FROM exams_supervision_pers_info WHERE m_pers_id = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->id);
        $statement->execute();

        $result = $statement->get_result();
        if($result->num_rows > 0){
            for($i = 0; $i < $result->num_rows; $i++){
                $row = $result->fetch_assoc();
                $this->priority = $row["priority"];
            }
            return true;
        }else {
            $this->priority = 1;
            return false;
        }
    }

    /**
     * Sets all Locked Times for the Employee given in the $times
     * @param Array Array of Objects representing LockedTimes
     */
    public function SetLockedTimes($times){
        $Apps = Database::GetApps();

        for($i = 0; $i < count($times); $i+=1){
            $span = LockedTimeSpan::fromJSONObject($times[$i]);
            $span->Save();
        }
    }

    /**
     * Sets or updates the Priority of an Employee
     */
    public function SetPriority($newPriority){
        $Apps = Database::GetApps();

        
        if($this->GetEmployeeInfo()){
            $sql = "UPDATE `exams_supervision_pers_info` SET `priority`= ? WHERE `m_pers_id` = ?";
        }else{
            $sql = "INSERT INTO `exams_supervision_pers_info`(`priority`, `m_pers_id`) VALUES (?,?)";
        }

        $statement = $Apps->prepare($sql);
        $statement->bind_param("ii", $newPriority, $this->id);
        return $statement->execute();
    }

    /**
     * Checks if an Employee is available for the given Exam
     * @param Exam Exam to be checked for
     * @return Bool True if the Employee is available, false otherwise.
     */
    public function isAvailable($exam) : bool{
        
        if($this->priority == 0) return false;
        if($this->lockedTimes == null) $this->GetLockedTimes();
        if($this->plannedSupervisions == null) $this->GetPlannedSupervisions();


        $ExamStart = $exam->GetStartDateTime()->getTimestamp();
        $ExamEnd   = $exam->GetEndDateTime()->getTimestamp();

        if(count($this->lockedTimes) != 0){
            for($i = 0; $i < count($this->lockedTimes); $i++){
                $lockedStart = $this->lockedTimes[$i]->GetStartDateTime()->getTimestamp();
                $lockedEnd   = $this->lockedTimes[$i]->GetEndDateTime()->getTimestamp();

                if(($lockedStart <= $ExamStart && $ExamStart <= $lockedEnd) ||
                ($ExamStart <= $lockedStart && $lockedStart <= $ExamEnd)){
                    return false;
                }
            }
        }

        if(count($this->plannedSupervisions) != 0){
            for($i = 0; $i < count($this->plannedSupervisions); $i++){
                $lockedStart = $this->plannedSupervisions[$i]->GetStartDateTime()->getTimestamp();
                $lockedEnd   = $this->plannedSupervisions[$i]->GetEndDateTime()->getTimestamp();

                if(($lockedStart <= $ExamStart && $ExamStart <= $lockedEnd) ||
                ($ExamStart <= $lockedStart && $lockedStart <= $ExamEnd)){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Writes down the Object as JSON string.
     * @return {String} Json compatible string representation of the Object
     */
    public function toJSON() : string{
        $json = array();
        $json["id"]         = $this->id;
        $json["first_name"] = $this->first_Name;
        $json["last_name"]  = $this->last_Name;
        $json["title"]      = $this->title;
        $json["email"]      = $this->email;
        $json["faculty"]    = $this->fak;
        $json["priority"]   = $this->priority;

        return json_encode($json);
    }
}