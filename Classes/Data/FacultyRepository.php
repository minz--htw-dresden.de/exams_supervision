<?php
namespace AppsTeam\ExamsSupervision\Data;

use AppsTeam\ExamsSupervision\Database\Database;
use AppsTeam\ExamsSupervision\Data\Faculty;
use AppsTeam\ExamsSupervision\Data\Repository;

class FacultyRepository{
    private $repo = array();

    /**
     * Gets the Instance of this Class or creates one
     * @return Repository Instance of Class
     * @todo Check if this class is the best approach
     */
    /*public static function GetInstance(){
        if(FacultyRepository::$instance == null){
            FacultyRepository::$instance = new FacultyRepository();
        }
        return FacultyRepository::$instance;
    }*/

    /**
     * Gets and Returns all Faculties from DB
     * @return Array Array of Faculties
     */
    public function GetAll() : ?array{
        if($this->repo != null) return $this->repo;

        $db = Database::GetApps();
        $sql = "SELECT * FROM `m_orgeinheit` WHERE `ist_fakultaet` = 1";
        $statement = $db->prepare($sql);
        $statement->execute();
        $result = $statement->get_result();
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            array_push($this->repo, Faculty::CreateAssoc($row));
        }
        return $this->repo;
    }
 
    /**
     * Gets a Faculty by its m_orgeinheit_id
     * @param Int ID
     * @return Faculty
     */
    public function findById($id) : ?Faculty{
        if(count($this->repo) == 0) $this->GetAll();
        
        foreach($this->repo as $fak){
            if($fak->getFak_id() == $id) return $fak;
        }

        return null;
    }

    /**
     * Adds a single Item to the repository
     */
    function add($item){
        array_push($this->repo, $item);
    }

    /**
     * @return Array returns the Repo
     */
    function getRepo() : ?array{ return $this->repo;}

    /**
     * Writes down the Object as JSON string.
     * @return String Json compatible string representation of the Object
     */
    public function toJSON() : string{
        $res = "[";
            
        for($i = 0; $i < count($repo); $i++){
            $res.= $repo[$i]->toJSON();
            
            if($i < count($repo)){
                $res.= ",";
            }
        }
        $res.= "]";
        return $res;
    }
}