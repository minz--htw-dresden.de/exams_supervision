<?php
namespace AppsTeam\ExamsSupervision\Data;

use AppsTeam\ExamsSupervision\Database\Database;
use AppsTeam\ExamsSupervision\Data\Employee;
use AppsTeam\ExamsSupervision\Data\EmployeeRepository;

/**
 * Class wich holds all infos for Exams
 */
class Exam{

    /**
     * The ID of the active Plan (Active Semester)
     */
    static protected $activePlanID = -1;

    /**
     * Database ID of the Exam
     */
    protected $id;

    /**
     * The ID of the Plan (Semester) this Exam is assigned to
     */
    protected $planID;

    /**
     * Date of the Exam in YYYY-MM-DD
     */
    protected $date;

    /**
     * Start time of the Exam in HH:mm:ss
     */
    protected $start;

    /**
     * End time of the Exam in HH:mm:ss
     */
    protected $end;

    /**
     * String of all Rooms in wich the Exam will take place
     */
    protected $rooms;

    /**
     * Complete name of the modul
     */
    protected $modul;

    /**
     * Type of the Exam (e.g. SP / NW)
     */
    protected $type;

    /**
     * Time of the Exam in Minutes
     */
    protected $length;

    /**
     * Array of Examiners wich hold the Exam
     * @see {Examiner}
     */
    protected $examiner;

    /**
     * Array of Employees assigned to serve as Supervisors
     */
    protected $supervisors;

    /**
     * Planning status of this exam
     * 0 -> Not Planned
     * 1 -> Planned (not ready)
     * 2 -> Planned (estimated supervisions count is reached)
     * 3 -> Ready as defined by planner
     */
    protected $status;

    /**
     * Wich Faculty should write the plan?
     */
    protected $planningFaculty;

    /**
     * Faculty the Modul is presented in
     */
    protected $faculty;

    /**
     * ID to collapse 
     */
    protected $mergeString;

    /**
     * Modulname getter
     * @return String
     */
    public function getModul():string{return $this->modul;}

    /**
     * ID getter
     * @return Int
     */
    public function getID():int{return $this->id;}

    /**
     * Room getter
     * @return String
     */
    public function getRooms():string{return $this->rooms;}

    /**
     * 
     */
    public function getPlanningFaculty():int{return $this->planningFaculty;}
    public function getFaculty():int{return $this->faculty != 0 ? $this->faculty : $this->planningFaculty;}

    public function getMergeString():string{return $this->mergeString != NULL ? $this->mergeString : microtime();}

    /**
     * Returns the Count of Examiners
     * @return Int
     */
    public function getExaminerCount():int{
        if($this->examiner == null) $this->GetExaminer();
        return count($this->examiner);
    }

    public function HasSharedSupervisorsPending():bool{
        //var_dump($this->GetSharedSupervisors()->GetCount());
        return $this->GetSharedSupervisors()->GetCount() > 0;
    }

    public function setPlanned($status){
        $this->status = $status;
    }

    /**
     * Returns the Names of all Examiners seperated by '/'
     * @return {String}
     */
    public function getExaminerString():string{
        if($this->examiner == null) $this->GetExaminer();
        $names = array();
        for($i = 0; $i < count($this->examiner); $i++){
            array_push($names, $this->examiner[$i]->GetLastName());
        }
        return implode("/", $names);
    }

    public function GetActivePlanID(){
        $Apps = Database::GetApps();
        $sql = "SELECT `exams_planner_plan_id` FROM `exams_planner_plan` WHERE `ist_aktiv` = 1";
        $statement = $Apps->prepare($sql);
        $statement->execute();
        $result = $statement->get_result();
        $row = $result->fetch_assoc();
        Exam::$activePlanID = $row["exams_planner_plan_id"];
    }

    /**
     * @param {Array} Assoc Array for the Exam as retrieved by the Database
     * @return {Exam}
     * @static
     */
    public static function CreateAssoc($assoc) : Exam{
        $exam = new Exam();
        $exam->id       = $assoc["exams_planner_pruefung_id"];
        $exam->date     = $assoc["datum"];
        $exam->start    = $assoc["zeit_von"];
        $exam->end      = $assoc["zeit_bis"];
        $exam->rooms    = $assoc["raum"];
        $exam->modul    = $assoc["modul"];
        $exam->type     = $assoc["art"];
        $exam->length   = $assoc["dauer"];
        $exam->planID   = $assoc["fk_exams_planner_plan_id"];
        $exam->mergeString = $assoc["zusammenlegung"];
        $exam->planningFaculty = $assoc["fakultaet_verantw"];
        $exam->faculty = $assoc["fakultaet"];

        $exam->GetExamInfo();
        return $exam;
    }

    /**
     * @param {Int} id The ID of the Exam to get from the Database
     * @return {Exam} if found, NULL otherwise
     * @static
     * @todo This function should return NULL if the Exam is not found
     */
    public static function GetExamById($id) : ?Exam{
        $Apps = Database::GetApps();
        $sql = "SELECT * FROM `exams_planner_pruefung` WHERE `exams_planner_pruefung_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $id);
        $statement->execute();
        $result = $statement->get_result();
        $row = $result->fetch_assoc();
        return Exam::CreateAssoc($row);
    }

    /**
     * Determines if a Exam record is considered Valid
     * @return {Boolean} True is all nessecarry data is available
     */
    public function isValid() : bool{
        if(Exam::$activePlanID == -1) $this->GetActivePlanID();
        return $this->date != null && $this->start != null && $this->end != null && $this->rooms != null && $this->planID == Exam::$activePlanID;
    }

    public function isServiceModul() :bool{
        return $this->planningFaculty != $this->getFaculty();
    }

    /**
     * Gets all Examiners for this Exam from the Database
     * @return {Array} Array of Employee
     */
    public function GetExaminer() : ?array{
        $this->examiner = array();
        $Apps = Database::GetApps();
        $sql = "SELECT fk_exams_planner_pruefer_id FROM `exams_planner_pruefung_pruefer` WHERE `fk_exams_planner_pruefung_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->id);
        $statement->execute();
        $result = $statement->get_result();
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            /*Es gibt Prüfungen die Prüfer zugewiesen haben deren IDs nicht in der DB zu finden sind*/
            $emp = Employee::GetEmployeeByID($row["fk_exams_planner_pruefer_id"]);
            if($emp != null)
                array_push($this->examiner, $emp);
        }
        return $this->examiner;
    }

    /**
     * Checks if at least one of the Examiners for this Exam is belonging to the given Faculty
     * @return {Boolean} Returns true if at least one of the Examiners for this Exam is belonging to the given Faculty, false otherwise.
     */
    public function CheckForFaculty($fak) : bool{
        for($i = 0; $i < count($this->examiner); $i++){
            if($this->examiner[$i]->getFakID() == $fak) return true;
        }
        return false;
    }

    /**
     * Returns the Start of the Exam
     * @return {DateTime} Format Y-m-j H:i:s
     */
    public function GetStartDateTime() : \DateTime{
        return \DateTime::CreateFromFormat("Y-m-j H:i:s", $this->date." ".$this->start);
    }

    /**
     * Returns the End of the Exam
     * @return {DateTime} Format Y-m-j H:i:s
     */
    public function GetEndDateTime() : \DateTime{
        return \DateTime::CreateFromFormat("Y-m-j H:i:s", $this->date." ".$this->end);
    }

    /**
     * Adds the Employee with the given ID as Supervisor to the Exam
     * @return {Boolean} Returns if the SQL statement could be executed properly
     */
    public function AddSupervisor($id):bool{
        $sql = "INSERT INTO `exams_supervision_supervisor` (`m_pers_id`, `exams_planner_pruefung_id`) VALUES (?, ?)";
        $Apps = Database::GetApps();

        $statement = $Apps->prepare($sql);
        $statement->bind_param("ii", $id, $this->id);
        $res = $statement->execute();
        if($this->status != 3){
            if($this->getSupervisors()->getCount() >= count(explode("/", $this->rooms)))
                $this->status = 2;
            else
                $this->status = 1;
        }
        return $res;
    }

    public function RemoveSupervisor($id):bool{
        $Apps = Database::GetApps();
        $sql = "DELETE FROM `exams_supervision_supervisor` WHERE `m_pers_id` = ? AND `exams_planner_pruefung_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("ii", $id, $this->id);
        $res = $statement->execute();
        if($this->status != 3){
            if($this->getSupervisors()->getCount() >= count(explode("/", $this->rooms)))
                $this->status = 2;
            else
                $this->status = 1;
        }
        return $res;
    }

    public function ClearSupervisors(){
        $Apps = Database::GetApps();
        $sql = "DELETE FROM `exams_supervision_supervisor` WHERE `exams_planner_pruefung_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->id);
        $statement->execute();
    }

    /**
     * Gets all Employees wich should be supervisors for this Exam from the Database
     * @return {Array} Array of Employee
     */
    public function GetSupervisors():EmployeeRepository{
        $sql = "SELECT * FROM `exams_supervision_supervisor` WHERE `exams_planner_pruefung_id` = ?";
        $Apps = Database::GetApps();

        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->id);
        $statement->execute();
        $result = $statement->get_result();
        $supervisors = [];
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            array_push($supervisors, Employee::GetEmployeeByID($row["m_pers_id"]));
        }
        $this->supervisors = new EmployeeRepository();
        $this->supervisors->SetEmployees($supervisors);
        return $this->supervisors;
    }

    public function GetSharedSupervisors(): EmployeeRepository{
        $sql = "SELECT * from `exams_supervision_share` WHERE `fk_share_exam_id`= ?";
        $Apps = Database::GetApps();

        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->getID());
        $statement->execute();
        $result = $statement->get_result();
        $supervisors = [];
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            array_push($supervisors, Employee::GetEmployeeByID($row["m_pers_id"]));
        }
        $s = new EmployeeRepository();
        $s->SetEmployees($supervisors);
        return $s;
    }

    public function GetExamInfo(){
        $Apps = Database::GetApps();
        $sql = "SELECT * FROM `exams_supervision_exam_info` WHERE `fk_exams_planner_pruefung_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->id);
        $statement->execute();
        $result = $statement->get_result();
        if($result->num_rows == 0){
            $this->status = 0;
            return;
        }
        $row = $result->fetch_assoc();
        $this->status = $row["status"];
    }

    public function SaveExamInfo(){
        $Apps = Database::GetApps();
        $sql = "SELECT * FROM `exams_supervision_exam_info` WHERE `fk_exams_planner_pruefung_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->id);
        $statement->execute();
        $result = $statement->get_result();
        if($result->num_rows == 0){
            $sql = "INSERT INTO `exams_supervision_exam_info`(`status`, `fk_exams_planner_pruefung_id`) VALUES (?, ?)";
        }else{
            $sql = "UPDATE `exams_supervision_exam_info` SET `status`= ? WHERE `fk_exams_planner_pruefung_id` = ?";
        }
        $statement = $Apps->prepare($sql);
        $statement->bind_param("ii", $this->status, $this->id);
        $statement->execute();
    }

    /**
     * Adds an Shareentry for the given exam
     * This gives the planning faculty the possibility to use this employee as supervisor
     * @param Int shareWith : ExamID to share the Employee for
     */
    function addShare($share){
        $Apps = Database::GetApps();
        $sql = "INSERT INTO `exams_supervision_share` (`m_pers_id`, `fk_share_exam_id`) VALUES (?, ?)";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("ii", $share, $this->getID());
        return $statement->execute();
    }

    function removeShare($removeShare){
        $Apps = Database::GetApps();
        $sql = "DELETE FROM `exams_supervision_share` WHERE m_pers_id = ? AND fk_share_exam_id = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("ii", $removeShare, $this->getID());
        return $statement->execute();
    }

    /**
     * Writes down the Object as JSON string.
     * @return {String} Json compatible string representation of the Object
     */
    public function toJSON() : string{
        $json = array();
        $json["id"]       = $this->id;
        $json["datum"]    = $this->date;
        $json["zeit_von"] = $this->start;
        $json["zeit_bis"] = $this->end;
        $json["raum"]     = $this->rooms;
        $json["modul"]    = $this->modul;
        $json["art"]      = $this->type;
        $json["dauer"]    = $this->length;
        $json["status"]   = $this->status;
        $json["faculty"]      = $this->getFaculty();
        $json["planning_faculty"]  = $this->planningFaculty;
        $json["hasSharedSupervisors"] = $this->HasSharedSupervisorsPending();
        $json["mergeString"] = $this->getMergeString();
        $json["service"] = $this->isServiceModul();
        $json["Examiner"] = "[";
        
            if($this->examiner != null){
                for($i = 0; $i < count($this->examiner); $i++){
                    $json["Examiner"].= $this->examiner[$i]->toJSON();
                    
                    if($i < count($this->examiner)-1){
                        $json["Examiner"].= ",";
                    }
                }
            }
            $json["Examiner"].= "]";

        return json_encode($json);
    }
}