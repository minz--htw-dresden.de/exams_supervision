<?php
namespace AppsTeam\ExamsSupervision\Data;

use AppsTeam\ExamsSupervision\Database\Database;
use AppsTeam\ExamsSupervision\Data\Exam;
use AppsTeam\ExamsSupervision\Data\Repository;
use AppsTeam\ExamsSupervision\Data\EmployeeRepository;

/**
 * Extends the Repository class for Exams
 * @extends {Repository}
 */
class ExamsRepository{

    private $repo = array();

    /**
     * @param {Int} fak_id ID of the Faculty
     * @return {Array} Array of Exams
     */
    public function GetAllForFaculty($fak_id) : ?array{
        if($this->repo != null) return $this->repo;

        $db = Database::GetApps();

        $this->repo = array();
        $employees = new EmployeeRepository();
        $employees->GetEmployeesForFaculty($fak_id);
       
        $employees->SetEmployees(array_filter($employees->GetEmployees(), function ($emp){
            return $emp->IsDozent() || $emp->IsProfessor();
        }));

        foreach($employees->GetEmployees() as $emp){
            $exams = $emp->GetOwnExams();
            if(count($exams) > 0)
                $this->repo = array_merge($this->repo, $exams);
        }

        if(count($this->repo) > 1){
            $this->repo = $this->array_uunique($this->repo, function ($lhs, $rhs){
                return $lhs->GetID() <=> $rhs->GetID();
            });
        }

        foreach($this->repo as $exam) $exam->GetExaminer();

        return $this->repo;
    }

    /**
     * Adds a single Item to the repository
     */
    function add($item){
        array_push($this->repo, $item);
    }

    /**
     * @return Array returns the Repo
     */
    function getRepo() : ?array{ return $this->repo;}
    function setRepo($repo) {$this->repo = $repo;}

    /**
     * Gets the Count of items in the repo
     * @return Int Count of items
     */
    public function getCount() : int{ return count($this->repo);}

    public function sortById(){
        usort($this->repo, function($a, $b){
            return $a < $b;
        });
    }

    /**
     * Makes an array unique
     * https://www.php.net/manual/de/function.array-unique.php#122857
     */
    public function array_uunique(array $array, callable $comparator): array {
        $unique_array = [];
        do {
            $element = array_shift($array);
            $unique_array[] = $element;
    
            $array = array_udiff(
                $array,
                [$element],
                $comparator
            );
        } while (count($array) > 0);
    
        return $unique_array;
    }
    
    /**
     * Writes down the Object as JSON string.
     * @return {String} Json compatible string representation of the Object
     */
    public function toJSON(): string{
        $res = "[";
            
        if($this->repo != null){
            for($i = 0; $i < count($this->repo); $i++){
                $res.= $this->repo[$i]->toJSON();
                
                if($i < count($this->repo)-1){
                    $res.= ",";
                }
            }
        }
        $res.= "]";
        return $res;
    }
}