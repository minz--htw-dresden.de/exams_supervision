<?php

namespace AppsTeam\ExamsSupervision\Data;

class Attachment{
    private $path;
    private $filename;
    private $contents;
    private $contentType;

    public function GetContent(){
        return $this->contents;
    }

    public function GetFilename(){
        return $this->filename;
    }

    public function GetContentType(){
        return $this->contentType;
    }

    private function ReadContent(){
        if(file_exists($_SERVER['DOCUMENT_ROOT'] . $this->path))
            $this->contents = chunk_split(base64_encode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . $this->path)));
    }

    public function __construct($path, $contentType = null){
        $this->path = $path;
        $this->ReadContent();
        $parts = explode("/", $this->path);
        $this->filename = $parts[count($parts)-1];

        $parts = explode(".", $this->filename);
        switch($parts[count($parts)-1]){
            case "pdf": $this->contentType = "application/pdf";
                break;
            default: $this->contentType = "text/plain";
        }
    }
}

class Email{

    private $headers = "";
    private $from = "";
    private $to = "";
    private $message = "";
    private $subject = "";
    private $charset = "utf-8";
    private $contentType = "text/html";
    private $attachments = array();
    private $uid;

    public function SetHeaders($headers){$this->headers = $headers;}
    public function SetFrom($From){$this->from = $From;}
    public function SetTo($To){$this->to = $To;}
    public function SetMessage($Message){$this->message = $Message;}
    public function SetSubject($Subject){$this->subject = $Subject;}
    public function SetCharset($charset){$this->charset = $charset;}
    public function SetContentType($contentType){$this->contentType = $contentType;}

    public function AddAttachment($filename){
        array_push($this->attachments, new Attachment($filename));
    }

    public static function Prepare($From, $To, $Subject, $Message = ""){
        $mail = new Email();
        $mail->from = $From;
        $mail->to = $To;
        $mail->subject = $Subject;
        $mail->message = $Message;
        $mail->headers = array(
            'From' => $From,
            'To' => $To,
            'Content-Type' => $mail->contentType,
            'charset' => $mail->charset,
            'MIME-Version' => "1.0",
            'X-Mailer' => 'PHP/'.phpversion()
        );
        return $mail;
    }

    private function build(){
        $this->uid = md5(uniqid(time()));
        $this->SetHeaders(array(
            'From' => $this->from,
            'To' => $this->to,
            'Content-Type' => "multipart/mixed; boundary=\"PHP-mixed-".$this->uid."\"",
            'charset' => $this->charset,
            'MIME-Version' => "1.0",
            'X-Mailer' => 'PHP/'.phpversion()
        ));

        $message = $this->message; //buffer user message

        $this->message = "--PHP-mixed-".$this->uid."
Content-Type: multipart/alternative; boundary=\"PHP-alt-".$this->uid."\"
--PHP-alt-".$this->uid."
Content-Type: ".$this->contentType."; charset=".$this->charset."
Content-Transfer-Encoding: 7bit

".$message."


--PHP-mixed-".$this->uid."
Content-Type: ".$this->attachments[0]->GetContentType()."; name=\"".$this->attachments[0]->GetFilename()."\"
Content-Transfer-Encoding: base64
Content-Discription: attachment

".$this->attachments[0]->GetContent()."

--PHP-mixed-".$this->uid."--";
    }

    public function Send(){
        if(count($this->attachments) != 0)
            $this->build();

        mail($this->to, $this->subject, $this->message, $this->headers);
    }
}