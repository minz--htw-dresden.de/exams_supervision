<?php
namespace AppsTeam\ExamsSupervision\Data;

use AppsTeam\ExamsSupervision\Database\Database;
use AppsTeam\ExamsSupervision\Data\FacultyRepository;

/**
 * Front End User Class
 * Simple class to check User access
 */
class FEUser{

    /**
     * Users ID
     */
    private $UID;

    /**
     * Array of Faculties this user can work on.
     */
    private $faculties;

    public static $bypass = false;

    /**
     * @return Array of Faculties
     */
    function GetFaculties(){return $this->faculties;}

    function __construct(){
        if($GLOBALS['TSFE']->fe_user == null){
            $this->redirect();
            return;
        }
        $this->UID = $GLOBALS['TSFE']->fe_user->user['uid'];
        $this->faculties = new FacultyRepository();
        $this->GetAllowedFacultiesFromDB();
    }

    /**
     * Gets all Faculties, the user is allowed to work on, from DB 
     */
    private function GetAllowedFacultiesFromDB(){
        $Apps = Database::GetApps();

        $sql = "SELECT * FROM `exams_supervision_access` WHERE `typo3_uid` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->UID);
        $statement->execute();
        $result = $statement->get_result();
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            $this->faculties->add(Faculty::GetFacultyById($row["fk_orgeinheit_id"]));
        }
    }

    public function IsAllowed($fak){
        if(FEUser::$bypass === true) return true;
        foreach($this->faculties->GetRepo() as $f){
            if($f->getFak_id() == $fak){
                return true;
            }
        }
        return false;
    }

    private function redirect(){
        //TODO: kann ich den nutzer hier zu einer anmeldeseite umleiten? Ist das überhaupt nötig?
        die("User not logged in");
    }
}