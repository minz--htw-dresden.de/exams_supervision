<?php
namespace AppsTeam\ExamsSupervision\Data;

require(__DIR__."/../../Resources/Public/PHP/fpdf.php");

/**
 * Overrides the FPDF Class and applies some standard styling
 * @extends FPDF
 */
class FPDFproxy extends \FPDF{

    static $folder = "exams_supervision";

    function Header(){
        $this->Image(__DIR__."/../../Resources/Public/pics/logo_full.png", 10, 6, 70);
    }

    function Footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(30, 10, "Erstellt von: AppsTeam//ExamsSupervision");
    }

    function Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link=''){
        //Check if text is longer than w
        $length = $this->GetStringWidth($txt);
        while($length > $w){
            $txt = substr($txt, 0, strlen($txt) - 4)."...";
            $length = $this->GetStringWidth($txt);
        }
        parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
    }

    function generateExamReport($exam){
        $supervisors = $exam->GetSupervisors();

        $this->SetTextColor(0);
        
        $this->AddPage();
        $this->SetY(75);
        $this->SetFont("Arial", "B", 11);
        $this->Cell(55, 16, utf8_decode("Eingeteilte Aufsichten für: "));
        $this->Cell(220, 16, utf8_decode($exam->getModul()));
        $this->Ln(20);

        $this->SetFont("Arial", "", 8);
        $this->Cell(40, 12, "Title", "B", 0, "L");
        $this->Cell(40, 12, "Name", "B", 0, "L");
        $this->Cell(40, 12, "Vorname", "B", 0, "L");
        $this->Cell(50, 12, "Email", "B", 1, "L");

        if($supervisors->GetCount() > 0){
            foreach($supervisors->GetEmployees() as $emp){
                $this->Cell(40, 12, $emp->getTitle(), "B", 0, "L");
                $this->Cell(40, 12, $emp->getLastName(), "B", 0, "L");
                $this->Cell(40, 12, utf8_decode($emp->getFirstName()), "B", 0, "L");
                $this->Cell(50, 12, $emp->getEmail(), "B", 1, "L");
            }
        }else{
            $this->Cell(45, 16, utf8_decode("Keine Aufsichten geplant"));
        } 
        $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/pdfs/'. FPDFproxy::$folder . '/';
        $filename = "supervision_".time().".pdf";
        $this->Output("F", $path.$filename);
        return '/uploads/pdfs/'. FPDFproxy::$folder . '/'.$filename;
    }

    public function generateEmployeeReport($emp){
         
        $this->SetTextColor(0);
        
        $this->AddPage();
        $this->SetY(75);
        $this->SetFont("Arial", "B", 11);
        $this->Cell(45, 16, utf8_decode("Aufsichtsplan für: "));
        $this->SetFont("Arial", "B", 13);
        $this->Cell(200, 16, $emp->getTitle()." ".utf8_decode($emp->getFullName()));
        $this->Ln(20);

        $this->SetFont("Arial", "", 8);
        $this->Cell(60, 12, "Modul", "B", 0, "C");
        $this->Cell(20, 12, "Datum", "B", 0, "C");
        $this->Cell(10, 12, "Begin", "B", 0, "C");
        $this->Cell(10, 12, "Ende", "B", 0, "C");
        $this->Cell(30, 12, utf8_decode("Prüfer"), "B", 0, "C");
        $this->Cell(60, 12, utf8_decode("Räume"), "B", 1, "C");

        $exams = $emp->GetPlannedSupervisions();

        if(count($exams) > 0){
            foreach($exams as $exam){
                $this->Cell(60, 12, utf8_decode($exam->getModul()), "B", 0, "L");
                $this->Cell(20, 12, $exam->GetStartDateTime()->format("d-m-Y"), "B", 0, "C");
                $this->Cell(10, 12, $exam->GetStartDateTime()->format("H:i"), "B", 0, "C");
                $this->Cell(10, 12, $exam->GetEndDateTime()->format("H:i"), "B", 0, "C");
                $this->Cell(30, 12, utf8_decode($exam->getExaminerString()), "B", 0, "C");
                $this->Cell(60, 12, utf8_decode($exam->getRooms()), "B", 1, "L");
            }
        }else{
            $this->Cell(45, 16, utf8_decode("Keine Aufsichten geplant"));
        }   
        $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/pdfs/'. FPDFproxy::$folder . '/';
        $filename = "supervision_".time().".pdf";
        $this->Output("F", $path.$filename);
        return '/uploads/pdfs/'. FPDFproxy::$folder . '/'.$filename;
    }
}