<?php

namespace AppsTeam\ExamsSupervision\Data;

use AppsTeam\ExamsSupervision\Data\EmplyeeRepository;
use AppsTeam\ExamsSupervision\Data\ExamsRepository;
use AppsTeam\ExamsSupervision\Data\Exam;
use AppsTeam\ExamsSupervision\Database\Database;

class Faculty {

    protected $fak_id;
    protected $fak_bez;
    protected $fak_bez_short;
    protected $fak_bez_long;
    protected $fak_htw_key;

    protected $EmployeeRepo;
    protected $ExamRepo;

    public static function CreateAssoc($assoc) : Faculty{
        $fak = new Faculty();
        $fak->fak_id        = $assoc["m_orgeinheit_id"];
        $fak->fak_bez_short = $assoc["bez_kurz"];
        $fak->fak_bez       = $assoc["bez"];
        $fak->fak_bez_long  = $assoc["bez_lang"];
        $fak->fak_htw_key   = $assoc["htw_key"];
        return $fak;
    }

    /**
     * Gets an Faculty by its ID from DB
     * @param Int the m_orheinheit_id
     * @return Faculty
     */
    public static function GetFacultyById($Id) : Faculty{
        $Apps = Database::GetApps();
        $sql = "SELECT * FROM `m_orgeinheit` WHERE `m_orgeinheit_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $Id);
        $statement->execute();
        $result = $statement->get_result();
        $row = $result->fetch_assoc();
        return Faculty::CreateAssoc($row);
    }

    /**
     * Creates an Repository of Employees assigned to this Faculty
     * @return EmployeeRepository
     */
    public function getEmployees() : EmployeeRepository{
        $this->EmployeeRepo = new EmployeeRepository();
        $this->EmployeeRepo->GetEmployeesForFaculty($this->fak_id);
        return $this->EmployeeRepo;
    }

    public function getPlanningEmployees() : EmployeeRepository{
        $repo = new EmployeeRepository();
        $Apps = Database::GetApps();
        $sql = "SELECT * FROM `exams_supervision_access` WHERE `fk_orgeinheit_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->fak_id);
        $statement->execute();
        $result = $statement->get_result();
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            $sql = "SELECT m_pers_id FROM `m_pers` WHERE `typo3_uid` = ?";
            $statement = $Apps->prepare($sql);
            $statement->bind_param("i", $row["typo3_uid"]);
            $statement->execute();
            $res = $statement->get_result();
            $repo->add(Employee::GetEmployeeById($res->fetch_assoc()["m_pers_id"]));
        }
        return $repo;
    }

    /**
     * Faculty Bez getter
     * @return String
     */
    public function getFak_bez() : string{
        return $this->fak_bez;
    }

    /**
     * Faculty ID getter
     * @return Int
     */
    public function getFak_id() : int{
        return $this->fak_id;
    }

    public function getResponsibleExams():?ExamsRepository{
        $Apps = Database::GetApps();
        
        $sql = "SELECT `exams_planner_pruefung_id`, IFNULL(`zusammenlegung`, UUID()) as uuid FROM `exams_planner_pruefung` WHERE fakultaet_verantw = ? GROUP BY uuid";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("i", $this->fak_id);
        $statement->execute();

        $this->ExamRepo = new ExamsRepository();

        $result = $statement->get_result();
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            $exam = Exam::GetExamById($row["exams_planner_pruefung_id"]);
            if($exam->isValid())
                $this->ExamRepo->add($exam);
        }

        return $this->ExamRepo;
    }

    public function getReceivedServiceModules():ExamsRepository{
        $Apps = Database::GetApps();
        
        $sql = "SELECT `exams_planner_pruefung_id`, IFNULL(`zusammenlegung`, UUID()) as uuid FROM `exams_planner_pruefung` WHERE fakultaet = ? AND fakultaet_verantw != ? GROUP BY uuid";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("ii", $this->fak_id, $this->fak_id);
        $statement->execute();

        $this->ExamRepo = new ExamsRepository();

        $result = $statement->get_result();
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            $exam = Exam::GetExamById($row["exams_planner_pruefung_id"]);
            if($exam->isValid())
                $this->ExamRepo->add($exam);
        }

        return $this->ExamRepo;
    }

    /**
     * Writes down the Object as JSON string.
     * @return {String} Json compatible string representation of the Object
     */
    public function toJSON() : string{
        $json = array();
        $json["id"]        = $this->fak_id;
        $json["bez"]       = $this->fak_bez;
        $json["bez_short"] = $this->fak_bez_short;
        $json["bez_long"]  = $this->fak_bez_long;
        $json["htw_key"]   = $this->fak_htw_key;

        return json_encode($json);
    }
}