<?php
namespace AppsTeam\ExamsSupervision\Data;

use AppsTeam\ExamsSupervision\Database\Database;

/**
 * Class for Timespans to Lock a User
 * Users will not appear as Available Supervisor if the asked time is overlapping with LockedTime
 */
class LockedTimeSpan{

    /**
     * ID Of the Locked Entry in DB
     */
    protected $id;

    /**
     * ID of the Employee
     */
    protected $pers_id;

    /**
     * Date the Timespan Starts
     */
    protected $date_from;

    /**
     * Date the Timespan ends
     */
    protected $date_to;

    /**
     * Time the TimeSpan starts
     */
    protected $time_from;

    /**
     * Time the Timespan ends
     */
    protected $time_to;

    /**
     * Toggle if the Timespan includes the whole Day
     */
    protected $allDay;


    public function GetID(){return $this->id != null ? $this->id : -1;}

    /**
     * Creates an Instance from an associative Array
     * @param Array Array as retrieved from DB
     * @return LockedTimeSpan Instance
     */
    public static function CreateAssoc($assoc) : LockedTimeSpan{
        $span = new LockedTimeSpan();
        $span->id        = $assoc["id"];
        $span->pers_id   = $assoc["m_pers_id"];
        $span->date_from = $assoc["m_date_from"];
        $span->date_to   = $assoc["m_date_to"];
        $span->time_from = $assoc["m_time_from"] == null ? "00:00:00" : $assoc["m_time_from"];
        $span->time_to   = $assoc["m_time_to"] == null ? "23:59:00" : $assoc["m_time_to"];
        $span->allDay    = $assoc["m_all_day"];
        return $span;
    }

    /**
     * Creates an Instance from an JSON Object
     * @param Object Object created from JSON String
     * @return LockedTimeSpan Instance
     */
    public static function fromJSONObject($obj) : LockedTimeSpan{
        $span = new LockedTimeSpan();
        $span->id        = $obj->id;
        $span->pers_id   = $obj->m_pers_id;
        $span->date_from = $obj->m_date_from;
        $span->date_to   = $obj->m_date_to;
        $span->time_from = $obj->m_time_from == "" ? "00:00:00" : $obj->m_time_from;
        $span->time_to   = $obj->m_time_to   == "" ? "23:59:00" : $obj->m_time_to;
        $span->allDay    = $obj->m_all_day;
        return $span;
    }

    /**
     * Write the Timespan to DB
     */
    public function Save(){
        if($this->id != null){
            $this->Update();
        }else{
            $this->SaveAsNew();
        }
    }

    /**
     * Saves the Timespan as New Entry to the DB
     */
    private function SaveAsNew(){
        $sql = "INSERT INTO `exams_supervision_pers_locked` (`m_pers_id`, `m_date_from`, `m_date_to`, `m_time_from`, `m_time_to`, `m_all_Day`)
            VALUES (?,?,?,?,?,?)";
        
        $Apps = Database::GetApps();
        $statement = $Apps->prepare($sql);
        $statement->bind_param("issssi", 
            $this->pers_id,
            $this->date_from,
            $this->date_to,
            $this->time_from,
            $this->time_to,
            $this->allDay);
        if($statement->execute() == false){
            echo $statement->error;
        };

    }

    /**
     * Saves the Timespan as Update to the DB
     */
    private function Update(){

        $sql = "UPDATE `exams_supervision_pers_locked` SET
         `m_pers_id`= ?,`m_date_from`= ?,`m_date_to`= ?,`m_time_from`= ?,`m_time_to`= ?,`m_all_day`= ? WHERE id = ?";

        $Apps = Database::GetApps();
        $statement = $Apps->prepare($sql);
        $statement->bind_param("issssii", 
            $this->pers_id,
            $this->date_from,
            $this->date_to,
            $this->time_from,
            $this->time_to,
            $this->allDay,
            $this->id);
        $statement->execute();
    }

    /**
     * Returns Start time as DateTime
     * @return DateTime
     */
    public function GetStartDateTime() : \DateTime{
        return \DateTime::CreateFromFormat("Y-m-j H:i:s", $this->date_from." ".$this->time_from);
    }

    /**
     * Returns End time as DateTime
     * @return DateTime
     */
    public function GetEndDateTime() : \DateTime{
        return \DateTime::CreateFromFormat("Y-m-j H:i:s", $this->date_to." ".$this->time_to);
    }

    /**
     * Deletes the Entry with the given ID from the Database
     * @return Boolean Returns if the statement was executed successfully
     */
    public function deleteFromDB(){
        $Apps = Database::GetApps();
        $sql = "DELETE FROM `exams_supervision_pers_locked` WHERE `id` = ? AND `m_pers_id` = ?";
        $statement = $Apps->prepare($sql);
        $statement->bind_param("ii", $this->id, $this->pers_id);
        if($statement->execute() == false){
            return $statement->error;
        };
    }

    public function equals($rhs): bool{
        return $this->id == $rhs->GetID();
    }

    /**
     * Writes down the Object as JSON string.
     * @return String Json compatible string representation of the Object
     */
    public function toJSON() : string{
        $result = array();
        $result["id"]          = $this->id;
        $result["m_pers_id"]   = $this->pers_id;
        $result["m_date_from"] = $this->date_from;
        $result["m_date_to"]   = $this->date_to;
        $result["m_time_from"] = $this->time_from;
        $result["m_time_to"]   = $this->time_to;
        $result["m_all_day"]   = $this->allDay;
        return json_encode($result);
    }
}