<?php

namespace AppsTeam\ExamsSupervision\Data;

use AppsTeam\ExamsSupervision\Database\Database;
use AppsTeam\ExamsSupervision\Data\Employee;
use AppsTeam\ExamsSupervision\Data\Exam;

class EmployeeRepository{

    protected $employees = array();

    public function GetEmployees(){ return $this->employees;}
    public function SetEmployees($employees){ $this->employees = $employees;}

    public function __construct(){}

    /**
     * Gets All Employees assigned to a Faculty
     * @param Int| Faculty ID
     */
    public function GetEmployeesForFaculty($faculty = 0){
        $App = Database::GetApps();

        if($this->employees == null) $this->employees = array();
        $condition = $faculty != 0 ? "fk_m_orgeinheit_id = ?" : "1";
        $sql = "SELECT * FROM m_pers WHERE ".$condition." AND inaktiv = 0";
        $statement = $App->prepare($sql);
        if($faculty != 0){
            $statement->bind_param("i", $faculty);
        }
        $statement->execute();
        $result = $statement->get_result();
        for($i = 0; $i < $result->num_rows; $i++){
            $row = $result->fetch_assoc();
            array_push($this->employees, Employee::CreateAssoc($row));
        }
    }

    /**
     * Filters all Employees by the given Exam
     * Deletes every Employee wich isnt available at the Exam
     */
    public function FilterLockedEmployees($exam){
        $new = array();
        foreach($this->employees as $emp){
            if($emp->isAvailable($exam)){
                array_push($new, $emp);
            }
        }
        $this->employees = $new;
    }

    /**
     * Merges two Repos
     */
    public function Merge($repo){
        $this->employees = array_merge($this->employees, $repo->GetEmployees());
    }

    public function Remove($id){
        $this->employees = array_filter($this->employees, function ($element){
            return $element->GetID() != $id;
        });
    }

    public function GetCount(){
        return count($this->employees);
    }

    /**
     * Adds a single Item to the repository
     */
    function add($item){
        array_push($this->employees, $item);
    }

    public function array_uunique(array $array, callable $comparator): array {
        $unique_array = [];
        do {
            $element = array_shift($array);
            $unique_array[] = $element;
    
            $array = array_udiff(
                $array,
                [$element],
                $comparator
            );
        } while (count($array) > 0);
    
        return $unique_array;
    }

    /**
     * Sorts the Repo by the Name of Employees
     */
    public function SortByName(){
        usort($this->employees, function ($a, $b){
            return strcmp($a->GetFullName(), $b->GetFullName());
        });
    }

    /**
     * Writes down the Object as JSON string.
     * @return String Json compatible string representation of the Object
     */
    public function toJSON() : string{
        $res = "[";
        
        if($this->employees != null){
            for($i = 0; $i < count($this->employees); $i++){
                $res.= $this->employees[$i]->toJSON();
                
                if($i < count($this->employees)-1){
                    $res.= ",";
                }
            }
        }
        $res.= "]";
        return $res;
    }
}