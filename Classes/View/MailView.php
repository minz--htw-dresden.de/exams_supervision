<?php

namespace AppsTeam\ExamsSupervision\View;

use TYPO3\CMS\Extbase\Mvc\View\AbstractView;
use TYPO3\CMS\Core\Mail\MailMessage;
use AppsTeam\ExamsSupervision\Data\FPDFProxy as FPDF;
use AppsTeam\ExamsSupervision\Data\Exam;
use AppsTeam\ExamsSupervision\Data\Employee;
use AppsTeam\ExamsSupervision\Data\Email;

use TYPO3\CMS\Fluid\View\TemplateView;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class MailView extends AbstractView{

    private $templatePath = "/Resources/Private/Templates/Email/";

    public function injectPersistenceManager(\TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface $persistenceManager)
     {
        $this->persistenceManager = $persistenceManager;
     }
 
     public function injectReflectionService(\TYPO3\CMS\Extbase\Reflection\ReflectionService $reflectionService)
     {
        $this->reflectionService = $reflectionService;
     }
 
     public function setVariablesToRender(array $variablesToRender)
     {
        $this->variablesToRender = $variablesToRender;
     }
 
     public function setConfiguration(array $configuration)
     {
        $this->configuration = $configuration;
     }

     public function getEmailTemplatePath($filename){
        return ExtensionManagementUtility::extPath('exams_supervision').$this->templatePath.$filename;
     }

     public function employeeReport(){
      $employee = Employee::GetEmployeeByID($this->variables["empid"]);
      $file = new FPDF();
      $path = $file->generateEmployeeReport($employee);

      
      $fluid = new TemplateView($this->context);
      $fluid->setTemplatePathAndFilename($this->getEmailTemplatePath("EmployeeReport.html"));
      $fluid->assign("Employee", $employee);

      $body = $fluid->render();

      $this->sendMail(array($employee->getEmail() => $employee->getFullName()), $body, array(0 => $path));
     }

     public function examReport(){
      $exam = Exam::GetExamById($this->variables["examid"]);
      $examiner = $exam->GetExaminers();
      $file = new FPDF();
      $path = $file->generateExamReport($exam);

      $fluid = new TemplateView($this->context);
      $fluid->setTemplatePathAndFilename($this->getEmailTemplatePath("ExamReport.html"));
      $fluid->assign("Exam", $exam);

      foreach ($examiner as $emp) {
         $fluid->assign("Employee", $emp);
         $body = $fluid->render();
         $this->sendMail(array($emp->getEmail() => $emp->getFullName()), $body, array(0 => $path));
      }
     }

     public function fromTemplate($template){
      $exam = Exam::GetExamById($this->variables["examid"]);
      $employee = Employee::GetEmployeeByID($this->variables["empid"]);

      $fluid = new TemplateView($this->context);
      $fluid->setTemplatePathAndFilename($this->getEmailTemplatePath("ExamReport.html"));
      $fluid->assign("Exam", $exam);
      $fluid->assign("Employee", $employee);

      $body = $fluid->render();
      $this->sendMail(array($employee->getEmail() => $employee->getFullName()), $body);
     }

    public function render(){
      switch($this->variables["type"]){
         case "exam":
            $report = $this->examReport();
            break;
         case "employee":
            $report = $this->employeeReport();
            break;
         case "template":
            $report = $this->fromTemplate($this->variables["template"]);
         default:
            return;
      }
    }

    private function sendMail($to, $body, $pathToAttachments = array()){
      $mail = new MailMessage();
      $mail 
      ->setFrom(array('apps@htw-dresden.de' => 'Prüfungsaufsichtsplanung-Systemnachricht'))
      ->setTo($to)
      ->setReturnPath('apps@htw-dresden.de')
      ->setSubject('Prüfungsaufsichten')
      ->addPart($body, "text/html");

      foreach($pathToAttachments as $path){
         $mail->attach(\Swift_Attachment::fromPath($_SERVER['DOCUMENT_ROOT'] . $path));
      }
      $mail->send();
    }
}