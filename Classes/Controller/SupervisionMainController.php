<?php

namespace AppsTeam\ExamsSupervision\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use AppsTeam\ExamsSupervision\Database\Database;
use AppsTeam\ExamsSupervision\Data\FacultyRepository;
use AppsTeam\ExamsSupervision\Data\FEUser;

/**
 * Extensions Main Controller
 * 
 * This controller only shows the index Action
 */
class SupervisionMainController extends ActionController{

    protected $facultyRepository;

    public function indexAction(){
        if($this->settings['bypass_user_access_for_faculties'] == "true"){
            $facultyRepository = FacultyRepository::GetInstance();
            $faculties = $facultyRepository->GetAll();
            $this->view->assign('Faculties', $faculties);
            FEUser::$bypass = true;
        }else{
            $user = new FEUser();
            $this->view->assign('Faculties', $user->GetFaculties()->getRepo());
        }
    }
}