<?php
namespace AppsTeam\ExamsSupervision\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use AppsTeam\ExamsSupervision\Data\FacultyRepository;
use AppsTeam\ExamsSupervision\Data\ExamsRepository;
use AppsTeam\ExamsSupervision\Data\EmployeeRepository;
use AppsTeam\ExamsSupervision\Data\LockedTimeSpan;
use AppsTeam\ExamsSupervision\Data\Employee;
use AppsTeam\ExamsSupervision\Data\Exam;
use AppsTeam\ExamsSupervision\Data\Faculty;
use AppsTeam\ExamsSupervision\Data\FEUser;

use AppsTeam\ExamsSupervision\View\MailView;

/**
 * The JSONAPI controller is the Main controller for processing Data between the Database and the Website
 */
class JSONAPIController extends ActionController{
    protected $defaultViewObjectName = \TYPO3\CMS\Extbase\Mvc\View\JsonView::class;

    protected $facultyRepository = null;
    protected $feUser = null;

    function initializeAction(){
        $this->feUser = new FEUser();
    }

    private function getArgument($name){
        if($this->request->hasArgument($name)){
            return $this->request->getArgument($name);
        }
        return null;
    }

    private function returnForbidden(){
        http_response_code(403);
        return "[]";
    }

    public function getEmployeesAction(){
        $this->facultyRepository = new FacultyRepository();
        $this->facultyRepository->GetAll();
        $fak_id = $this->getArgument("fak_id");

        if($this->feUser->IsAllowed($fak_id) === false) return $this->returnForbidden();

        $fak = $this->facultyRepository->findById($fak_id);
        
        if($fak == null) return "Faculty not found!";
        return $fak->getEmployees()->toJSON();
    }

    public function getLockedTimesAction(){
        $emp_id = $this->getArgument("emp_id");
        $employee = Employee::GetEmployeeByID($emp_id);
        $times = $employee->GetLockedTimes();

        if(!$this->feUser->IsAllowed($employee->getFakID())) return $this->returnForbidden();

        $res = "[";
        for($i = 0; $i<count($times); $i++){
            $res .= $times[$i]->toJSON();
            if($i<count($times)-1) $res .=",";
        }
        return $res."]";
    }

    public function setLockedTimesAction(){
        $emp_id = $this->getArgument("emp_id");
        $lockedTimes = $this->getArgument("lockedTimes");
        $employee = Employee::GetEmployeeByID($emp_id);
        if(!$this->feUser->IsAllowed($employee->getFakID())) return $this->returnForbidden();
        $dec = json_decode($lockedTimes);
        $employee->SetLockedTimes($dec);
    }

    public function deleteLockedTimeAction(){
        $lt_id = $this->getArgument("lt_id");
        $emp_id = $this->getArgument("emp_id");
        $employee = Employee::GetEmployeeByID($emp_id);
        if(!$this->feUser->IsAllowed($employee->getFakID())) return $this->returnForbidden();

        $ar = array();
        $ar["id"] = $lt_id;
        $ar["m_pers_id"] = $emp_id;
        $searchedLockedTime = LockedTimeSpan::CreateAssoc($ar);
        return $searchedLockedTime->deleteFromDB();
    }

    public function getExamsForFacultyAction(){
        $fak_id = $this->getArgument("fak_id");
        if($this->feUser->IsAllowed($fak_id) === false) return $this->returnForbidden();
        $fak = Faculty::GetFacultyById($fak_id);
        $examsRepo = $fak->getResponsibleExams();
        foreach($examsRepo->GetRepo() as $exam) $exam->getExaminer();
        if($examsRepo->GetCount() > 1){
            /*$examsRepo->array_uunique($examsRepo->GetRepo(), function($lhs, $rhs) =>{
                return $lhs
            });*/
        }
        return $examsRepo->toJSON();
    }

    public function getOwnServiceModulesForFacultyAction(){
        $fak_id = $this->getArgument("fak_id");
        if($this->feUser->IsAllowed($fak_id) === false) return $this->returnForbidden();
        $fak = Faculty::GetFacultyById($fak_id);
        $exams = $fak->getReceivedServiceModules();
        array_map(function ($elem) {$elem->GetExaminer();}, $exams->getRepo());
        return $exams->toJSON();
    }

    public function getOtherServiceModulesForFacultyAction(){
        $fak_id = $this->getArgument("fak_id");
        $exams = new ExamsRepository();
        $faculty = Faculty::GetFacultyById($fak_id);
        foreach($faculty->getResponsibleExams()->getRepo() as $exam){
            if($exam->isServiceModul() == true && $exam->getPlanningFaculty() == $fak_id){
                $exams->add($exam);
            }
        }

        if($exams->getCount() > 1){
            $exams->sortById();
            /*$exams->setRepo($exams->array_uunique($exams->getRepo(), function ($a, $b){ 
                return $a->getMergeString() <=> $b->getMergeString();
            }));*/
        }
        array_map(function ($elem) {$elem->GetExaminer();}, $exams->getRepo());
        return $exams->toJSON();
    }

    public function getAvailableSupervisorsAction(){
        $exam_id = $this->getArgument("exam_id");
        $fak_id = $this->getArgument("fak_id");

        $this->facultyRepository = new FacultyRepository();
        $this->facultyRepository->GetAll();

        $exam = Exam::GetExamById($exam_id);
        
        $faculty = $this->facultyRepository->findById($fak_id);
        
        if($faculty != null){
            $resRepo = $faculty->getEmployees();
            $resRepo->FilterLockedEmployees($exam);
        }

        $resRepo->Merge($exam->GetSharedSupervisors());

        if($resRepo->GetCount() > 1){
            $resRepo->SetEmployees($resRepo->array_uunique($resRepo->GetEmployees(), function ($lhs, $rhs){
                return $lhs->getID() <=> $rhs->getID();
            }));
        }

        return $resRepo->toJSON();
    }

    public function setPlannedSupervisorsAction(){
        $exam_id = $this->getArgument("exam_id");
        $supervisors = $this->getArgument("supervisors");
        $status = $this->getArgument("status");
        $supervisors = json_decode($supervisors);

        $exam = Exam::GetExamById($exam_id);
        $exam->GetExaminer();
        $exam->ClearSupervisors();

        $exam->SetPlanned(intval($status));

        foreach($supervisors as $supervisor){
            $exam->AddSupervisor($supervisor);
        }
        $exam->SaveExamInfo();
        return $exam->toJSON();
    }

    public function setPlannedSharedSupervisorsAction(){
        $exam_id = $this->getArgument("exam_id");
        $supervisors = $this->getArgument("supervisors");
        $status = $this->getArgument("status");
        $supervisors = json_decode($supervisors);

        $exam = Exam::GetExamById($exam_id);
        $exam->GetExaminer();
        $exam->SetPlanned(intval($status));
        $exam->ClearSupervisors();

        foreach($supervisors as $supervisor){
            $exam->AddSupervisor($supervisor);
            $exam->removeShare($supervisor);
        }
        $exam->SaveExamInfo();

        return $exam->toJSON();
    }

    public function getPlannedSupervisorsAction(){
        $exam_id = $this->getArgument("exam_id");

        $exam = Exam::GetExamById($exam_id);
        $repo = $exam->GetSupervisors();
        return $repo->toJSON();
    }

    public function getSupervisorStatsAction(){
        $fak_id = $this->getArgument("fak_id");
        if($this->feUser->IsAllowed($fak_id) === false) return $this->returnForbidden();

        $empRepo = new EmployeeRepository();
        $empRepo->GetEmployeesForFaculty($fak_id);
        $empRepo->SortByName();

        $res = "[";
        $res .= implode(",", array_map(function ($elem){
            return "{\"ID\":\"".$elem->getID()."\",
                    \"Titel\":\"".$elem->getTitle()."\",
                    \"Name\":\"".$elem->getFullName()."\",
                    \"Prioritaet\":\"".$elem->getPriority()."\",
                    \"Anz\":\"".$elem->getPlannedSupervisionsCount()."\"}";
        }, $empRepo->GetEmployees()));
        return $res."]";
    }

    public function getPlannedSupervisionsForEmployeeAction(){
        $emp_id = $this->getArgument("emp_id");
        $employee = Employee::GetEmployeeByID($emp_id);
        if($this->feUser->IsAllowed($employee->getFakID()) === false) return $this->returnForbidden();

        $exams = $employee->GetPlannedSupervisions();
        array_walk($exams,function ($elem){
            $elem->GetExaminer();
        });
        $res = "[";
            $res .= implode(",", array_map(function ($elem){
                return $elem->toJSON();
            }, $exams));
        return $res."]";
    }

    public function setSupervisorPriorityAction(){
        $priority = $this->getArgument("priority");
        $emp_id = $this->getArgument("emp_id");
        
        $employee = Employee::GetEmployeeByID($emp_id);
        if($this->feUser->IsAllowed($employee->GetFakID()) === false) return $this->returnForbidden();

        return json_encode($employee->SetPriority($priority));
    }

    public function getFacultyStatsAction(){
        $fak_id = $this->getArgument("fak_id");

        $inst = Faculty::GetFacultyById($fak_id);
        $repo = $inst->getResponsibleExams();
        if($this->feUser->IsAllowed($fak_id) === false) return $this->returnForbidden();
        
        $withoutSupervisorCount = $repo->getCount();

        foreach($repo as $exam){
            if($exam->GetSupervisors()->getCount() > 0) $withoutSupervisorCount-=1;
        }

        return '{
            "ExamsCount":"'.$repo->getCount().'",
            "ExamsWithoutSupervisors":"'.$withoutSupervisorCount.'"
        }';
        
    }

    public function shareSupervisorForExamAction(){
        $exam_id = $this->getArgument("exam_id");
        $supervisors = $this->getArgument("supervisors");
        $supervisors = json_decode($supervisors);

        $exam = Exam::GetExamById($exam_id);

        foreach ($supervisors as $supervisor) {
            $exam->addShare($supervisor);
        }

        //Prepare an Email notification for all Planning Employees
        $fak = Faculty::GetFacultyById($exam->getPlanningFaculty());
        $empRepo = $fak->getPlanningEmployees();
        $view = new MailView();
        $view->setControllerContext($this->controllerContext);
        foreach($empRepo->GetEmployees() as $emp){
            $view->assignMultiple(
                array("examid" => $exam_id,
                  "empid" => $emp->getID(),
                  "type" => "template",
                  "template" => "SupervisorRequestNotification.html")
            );
            $view->render();
        }
    }
}