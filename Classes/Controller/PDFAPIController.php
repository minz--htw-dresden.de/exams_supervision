<?php
namespace AppsTeam\ExamsSupervision\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Controller for every PDF Action
 */
class PDFAPIController extends ActionController{

    protected $viewFormatToObjectNameMap = array(
        'pdf' => 'AppsTeam\ExamsSupervision\Classes\ViewHelper\PDFView'
    );

    public function createPDFAction(){        
        $emp_id = -1;
        $fak_id = -1;
        $exam_id = -1;
        $type = "faculty";

        if($this->request->hasArgument("empid")){
            $emp_id = $this->request->getArgument("empid");
        }

        if($this->request->hasArgument("examid")){
            $exam_id = $this->request->getArgument("examid");
        }

        if($this->request->hasArgument("fakid")){
            $fak_id = $this->request->getArgument("fakid");
        }

        if($this->request->hasArgument("type")){
            $type = $this->request->getArgument("type");
        }
        
        $this->view->assign("empid", $emp_id);
        $this->view->assign("fakid", $fak_id);
        $this->view->assign("examid", $exam_id);
        $this->view->assign("type", $type);

        $this->view->setControllerContext($this->getControllerContext());
    }

    public function sendEmailAction(){

    }
}