<?php
namespace AppsTeam\ExamsSupervision\ViewHelpers;

use AppsTeam\ExamsSupervision\Data\FPDFProxy as FPDF;
use AppsTeam\ExamsSupervision\Data\Employee;
use AppsTeam\ExamsSupervision\Data\Exam;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class PDFViewHelper extends AbstractViewHelper {
 
    static $folder = "exams_supervision";

    public function initializeArguments(){
        $this->registerArgument('EmployeeID', 'integer', '', false, -1);
        $this->registerArgument('ExamID', 'integer', '', false, -1);
        $this->registerArgument('FacultyID', 'integer', '', false, -1);
        $this->registerArgument('ReportType', 'string', '');
    }

    public static function checkFolder(){
        $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/pdfs/';
        if (!file_exists($path . PDFViewHelper::$folder)) {
            mkdir($path . PDFViewHelper::$folder, 0777, true);
        }
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        PDFViewHelper::checkFolder();
        $type = $arguments["ReportType"];

        switch($type){
            case "employee":
                if($arguments["EmployeeID"] != -1){
                    PDFViewHelper::generateEmployeeReport($arguments["EmployeeID"]);
                }else{
                    return null;
                }
                break;
            case "exam":
                if($arguments["ExamID"] != -1){
                    PDFViewHelper::generateExamReport($arguments["ExamID"]);
                }else{
                    return null;
                }
        }
    }

    private static function generateExamReport($ExamID){
        $exam = Exam::GetExamById($ExamID);
        
        $file = new FPDF();
        $path = $file->generateExamReport($exam);
        echo $path;
    }

    private static function generateEmployeeReport($EmployeeID){
        $emp = Employee::GetEmployeeByID($EmployeeID);
        $file = new FPDF();
        $path = $file->generateEmployeeReport($emp);
        echo $path;
        //echo '/uploads/pdfs/'. PDFViewHelper::$folder . '/'.$filename;
    }
}