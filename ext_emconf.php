<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Exams-Supervision',
    'description' => 'Planungen für Prüfungsaufsichten',
    'category' => 'plugin',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.9.99',
        ],
    ],
    'state' => 'alpha',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearCacheOnLoad' => true,
    'author' => 'André Minz',
    'author_email' => 'andre.minz@htw-dresden.de',
    'author_company' => '',
    'version' => '0.0.1',
    'autoload' => [
        'psr-4' => [
            'AppsTeam\\ExamsSupervision\\' => 'Classes'
        ],
    ],
);

?>