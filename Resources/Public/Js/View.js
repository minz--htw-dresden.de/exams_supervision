import CEvent from "./Utils/CEvent.js";

    /**
     * @description The base View Class for all Views of the Vewsite
     */
export default class View{

    /**
     * @static
     * @description The View wich is active at the Time
     */
    static active = null;

    /**
     * @static
     * @description Array with all views wich were created.
     */
    static views = [];

    /**
     * @static
     * Property to store the Faculty ID used to get Data from the Server.
     */
     static m_faculty = -1;

    constructor(){
        /**
         * Property to determine if the View is considered Dirty.
         */
        this.m_isDirty = false;

        /**
         * Property to determine if the View is considered Active.
         */
        this.m_isActive = false;

        /**
         * The Activator of the View
         */
        this.m_activator = null;

        /**
         * Event to be Called if a View is Dirty
         */
        this.onIsDirty = new CEvent();

        /**
         * Event to be called if a View becomes Active
         */
        this.onBecomeActive = new CEvent();

        this.ViewID = View.register(this);
    }

    /**
     * @description Registeres the given View and returns a ViewID
     * @param {View} view The view to be registered 
     * @returns The ID of the View corresponding to View.views
     */
    static register(view){
        var id = View.views.push(view)-1; //return ID, not new length
        if(View.active == null) this.activateView(id);
        return id;
    }

    /**
     * @description Sets the Faculty ID for all registered Views.
     * @param {Int} fak The ID of the Faculty 
     */
    static setFacultyGlobal(fak){
        View.m_faculty = fak;
    }

    /**
     * @description Calls the Update Method for all registered Views.
     */
    static updateGlobal(){
        View.views.forEach(elem => elem.update());
    }

    /**
     * @description Resets all registered Views
     */
    static resetViewGlobal(){
        View.views.forEach(elem => elem.resetView());
    }

    /**
     * @description Activates the View with the given ID.
     * @param {Int} viewID The ID of the View to be activated.
     */
    static activateView(viewID){
        if(View.active != null)
            View.active.m_isActive = false;
        View.views[viewID].setActive(true);
        View.active = View.views[viewID];
    }

    /**
     * @description Adds a OnClick Listener to the given item to Activate the View.
     * @param {HTMLElement} htmlItem The Item wich "Activates" the View by Clicking on it.
     */
    registerActivator(htmlItem){
        var that = this;
        this.m_activator = htmlItem;
        this.m_activator.addEventListener("click", () => {View.activateView(that.ViewID);});
    }

    /**
     * @description Sets the ID of the Faculty.
     * @param {Int} f ID of a Faculty
     */
    setFaculty(f){
        this.m_faculty = f;
    }

    /**
     * @description Sets the isDirty property of the View.
     */
    setDirty(){
        this.m_isDirty = true;
    }

    /**
     * @description Sets the isActive property and Fires the onBecomeActive Event if bool is True.
     * @param {Boolean} bool Determines if the view should be set Active or not.
     */
    setActive(bool){
        if(bool){
            this.onBecomeActive.call();
        }
        this.m_isActive = bool;
    }

    /**
     * The Update funtion of the View.
     * Empty in this base class.
     */
    update(){}

    /**
     * This Function resets the View to inital.
     */
    resetView(){}

    /**
     * @description Checks if the View is dirty and Calls onIsDirty if so.
     * @returns The value of the isDirty Property
     */
    isDirty(){
        if(this.m_isDirty == true){
            this.onIsDirty.call();
            return true;
        }
        return false;
    }

    /**
     * 
     * @param {Event} event 
     * @returns Nothing
     */
    static onKeyDown(event){
        if(View.active == null) return;
        View.active.onKeyDown(event);
    }

    onKeyDown(event){}
}
