//Plugins.require(Globals.JsPath + "/Utils/FilterControl.js");
import FilterControl from "./FilterControl.js";

export default class InputFilterControl extends FilterControl{
    constructor(dataName, control, filter){
        super(dataName, control, filter);

        this.dataList = null;

        this.createDataList();
    }

    createDataList(){
        this.dataList = document.createElement("dataList");
        this.dataList.setAttribute("id", this.name + "_filter_dataList" + Date.now());
        this.dataList.setAttribute("for", this.control.id);

        this.control.setAttribute("list", this.dataList.id);

        document.body.appendChild(this.dataList);
    }

    getDataListId(){
        return this.dataList.id;
    }

    applyFilter(dataSets){
        var that = this;

        if(this.control.value == "") return dataSets;

        var newSet = dataSets.filter((elem) =>{
            return that.filterFunction(elem[this.name], that.control.value.toLowerCase());
        });
        return newSet;
    }

    /**
     * Sorts the Dataset with the given function
     * @param {Function} func Function(a,b):bool wich implements the sorting
     */
        sortFilterOptions(func){
        var options = jQuery("#" + this.dataList.id + " option");
        
        options.sort(function (a,b){
            return func(a,b);
        });

        jQuery(this.dataList).empty().append(options);
    }

    reset(){
        this.control.value = "";
    }

    clear(){
        this.dataList.replaceChildren();
    }

    /**
     * 
     * @returns Returns the selected HTMLOption Element
     */
    getSelectedOptionElement(){
        return this.dataList.querySelector("[value='" + this.control.value + "']");
    }

    getBestFittingOption(){
        var result = this.getSelectedOptionElement();
        var control = this.control;
        if(result != null) return result;

        if(this.control.value == ""){
            var option = this.dataList.querySelector("option");//just return the first option
            this.control.value = option.value;
            return option;
        }

        var options = Array.from(this.dataList.options).map(function(el){
            return el.value;
        });
        var relevantOptions = options.filter(function(option){
            return option.toLowerCase().includes(control.value.toLowerCase());
        }); 
        if(relevantOptions.length > 0){
            control.value = relevantOptions.shift();
        }else{
            control.value = options.shift();
        }

        return this.getSelectedOptionElement();
    }

    addFilterOption(value, caption = "", allow_duplicates = false){
        if(this.dataList.querySelector("[value='" + value+ "']") != null && !allow_duplicates){
            //console.log("Duplicate skiped: " + option + " for list: " + this.dataList.id);
            return null;
        }
        var opElem = document.createElement("option");
        if(caption == ""){
            opElem.setAttribute("data-value", value);
            opElem.setAttribute("value", value);
        }
        else{
            opElem.setAttribute("data-value", value);
            opElem.innerHTML = caption;
        }

        
        return this.dataList.appendChild(opElem);
    }

    static createControlGroup(name, control, filter){
        if(control == null){
            control = document.createElement("input");
        }

        control.classList.add("form-control");
        control.setAttribute("style", "background-color: #fff; border: solid 1px #e9ecef; padding: 0.5rem!important;");

        var group = document.createElement("div");
        group.classList.add("input-group");
        group.appendChild(control);

        var append = document.createElement("div");
        append.classList.add("input-group-append");

        var deleteButton = document.createElement("button");
        deleteButton.classList.add("btn");
        deleteButton.classList.add("btn-outline-secondary");
        deleteButton.classList.add("align-self-center");
        deleteButton.classList.add("d-flex");
        deleteButton.classList.add("close");
        deleteButton.type = "button";
        deleteButton.innerHTML = "<span aria-hidden='true'>&times;</span>";
        deleteButton.setAttribute("style", "padding: 0.5rem!important");

        append.appendChild(deleteButton);

        group.appendChild(append);

        var instance = new InputFilterControl(name, control, filter);

        deleteButton.onclick = () => {
            instance.reset();
            instance.onChange.call();
        }

        return {group: group, instance: instance};
    }
}
