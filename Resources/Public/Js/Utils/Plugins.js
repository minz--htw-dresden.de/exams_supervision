/**
 * A Class to Load Classes and Templates dynamically where they are needed.
 */
class Plugins{

    /**
     * @static
     * Array of all Loaded Templates
     */
    static loadedTemplates = [];

    /**
     * @static
     * Array of all Intervalls as created in Plugins.onIsLoaded
     * @see Plugins.onIsLoaded
     * @
     */
    static intervals = [];

    /**
     * @static
     * Object to Hold all loaded Classes for spped up Lookups and prevent double Loading
     */
    static _cls_ = {}; // serves as a cache, speed up later lookups

    /**
     * A function to load Files.
     * The Function creates an new syncronous XMLHttpRequest, loads and evaluates the file immediately.
     * @param {String} file Path of the file wich should be Loaded
     * @returns This Function returns nothing.
     * @static
     */
    static require(file){
        
        var parts = file.split('.');
        var name = parts[0].split('/');
        name = name[name.length-1];

        switch(parts[parts.length -1]){

            case "js":
                if(Plugins.getClass(name) != undefined) return;//We know this class already
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function(){
                    if(xmlhttp.status == 200 && xmlhttp.readyState == 4){
                        var a = eval(xmlhttp.responseText);
                        if(a != undefined){
                            
                            if(window.Framework == undefined){
                                window.Framework = {};
                            }
                            window[name] = a;
                        }
                    }
                };
                xmlhttp.open("GET",file,false);
                xmlhttp.send();
                break;
            case "css":
                Plugins.import(file);
                break;
            case "html":
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function(){
                    if(xmlhttp.status == 200 && xmlhttp.readyState == 4){
                        var a = xmlhttp.responseText;
                        document.body.innerHTML += a;
                    }
                };
                xmlhttp.open("GET",file,false);
                xmlhttp.send();
                break;
        }
    }

    /**
     * The Function creates an new asyncronous XMLHttpRequest, loads and evaluates the file when its available.
     * @param {String} file The File to be Loaded
     * @returns This function returns nothing.
     * @static
     */
    static async requireAsync(file){
        var parts = file.split('.');
        var name = parts[0].split('/');
        name = name[name.length-1];

        if(Plugins.getClass(name) != undefined) return;//We know this class already
        const response = await fetch(file);
        let txt = await response.text();
        var a;
        try{
            a = eval(txt);
        }catch(e){
            console.log(file);
            console.log(txt);
            console.log(e.message);
        }
        if(a != undefined){
            
            if(window.Framework == undefined){
                window.Framework = {};
            }
            window[name] = a;
        }
    }

    /**
     * Creates a new Header Element referencing the given File.
     * Loading and evaluating is up to the browser.
     * @param {String} file The File to be referenced.
     * @static
     */
    static import(file){
        var parts = file.split('.');
        switch(parts[parts.length -1]){
            case "js":
                var script = document.createElement("script");
                script.src = file;
                script.id = "Plugin:" + file;
                document.head.appendChild(script);
                break;
            case "css":
                var link = document.createElement("link");
                link.href = file;
                link.setAttribute("rel", "stylesheet");
                link.setAttribute("type", "text/css");
                link.id = "Plugin:" + file;
                document.head.appendChild(link);
                break;
        }
    }

    /**
     * Checks is a Class is loaded and ready to use.
     * @param {String} name Name of the Class.
     * @returns The Type of the Class if found, undefined otherwise.
     * @static
     * @throws {Error}
     */
    static getClass(name){
        if (!Plugins._cls_[name]) {
            // cache is not ready, fill it up
            if (name.match(/^[a-zA-Z0-9_.]+$/)) {
              // proceed only if the name is a single word string, namespaces are allowed
              try{
                Plugins._cls_[name] = eval(name);
              }catch(ReferenceError){
                    return undefined;
              }
            } else {
              // arbitrary code is detected 
              throw new Error("The name you tried to match is not allowed!");
            }
          }
          return Plugins._cls_[name];
    }

    /**
     * Creates an asynchronous Request to load a Template.
     * @param {String} filePath Path to the Template wich schould be loaded
     * @returns {HTMLCollection} The Children of the <Template> Node
     */
    static async getTemplate(filePath){
        const response = await fetch(filePath);

        var parts = filePath.split('.');
        var name = parts[0].split('/');
        name = name[name.length-1];

        let txt = await response.text();
        let html = new DOMParser().parseFromString(txt, "text/html");
        var template =  html.querySelector("head > template");
        Plugins.loadedTemplates.push(name);

        return template.content.children;
    }

    /**
     * 
     * @param {Array} names Array of Names for loaded Elements to check.
     * @returns True if all given elements were found, false otherwise.
     * @static
     */
    static checkForContent(names){
        var result = true;

        names.forEach((name) =>{
            var StepResult = false;
            if(Plugins.loadedTemplates.includes(name)){
                StepResult = true;
            }

            if(Plugins.getClass(name) != undefined){
                StepResult = true;
            }
            result &= StepResult;
        });

        return result;
    }

    /**
     * 
     * @param {Array} names Array of Names for loaded Elements to check.
     * @param {Function} callback Function to call is all elements were loaded.
     * @returns This Function returns nothing.
     */
    static onIsLoaded(names, callback){

        if(Plugins.checkForContent(names) == true){
            callback();
            return;
        }

        Plugins.intervals[names.join("")] = setInterval(() =>{
            if(Plugins.checkForContent(names) == true){
                clearInterval(Plugins.intervals[names.join("")]); 
                callback();
            }
        }, names, callback, 500);
    }
}