
    /*
        Litte Eventclass for calling functions from JS.
    */
export default class CEvent{
    constructor(){
        this.callbacks = [];
    }

    /**
    *    @description Adds a new Callback to the Event.
    *    @param {String}   name       Name of the callback.
    *    @param {Function} callback   Actual callback.
    */
    add(name,callback){
        this.callbacks.push({Name: name, Callback: callback});
    }

    /**
    *    @description Removes the callback with the given Name from the Event.
    *    @param {String} name    The Name of the Callback to be removed.
    */
    remove(name){
        this.callbacks = this.callbacks.filter(function(value){
            return value.Name != name;
        });
    }

    /**
     * @description Calls the Callbacks.
     */
    call(){
        this.callbacks.forEach((elem)=>{
            elem.Callback(arguments[0]);
        });
    }
}
