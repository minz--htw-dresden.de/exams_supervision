
class OrgEnum{
    static B = 1;
    static E = 2;
    static I = 3;
    static L = 4;
    static M = 5;
    static G = 6;
    static D = 7;
    static W = 8;
    static V = 9;
    static S = 13;
    static Z = 11;

    static IdToStr(id) {
        switch(id){
            case OrgEnum.B: return "Bauingeneurswesen";
            case OrgEnum.E: return "Elektrotechnik";
            case OrgEnum.I: return "Informatik/Mathematik";
            case OrgEnum.L: return "Landbau/Umwelt/Chemie";
            case OrgEnum.M: return "Maschienenbau";
            case OrgEnum.G: return "Geoinformation";
            case OrgEnum.D: return "Design";
            case OrgEnum.W: return "Witschaftswissenschaften";
            case OrgEnum.V: return "Verwaltung";
            case OrgEnum.S: return "Sprachenzentrum";
            case OrgEnum.Z: return "Zaft";
            default: return "Unbekannt";
        }
    }

    static IdToShortStr(id) {
        switch(id){
            case OrgEnum.B: return "B";
            case OrgEnum.E: return "E";
            case OrgEnum.I: return "I/M";
            case OrgEnum.L: return "L/U/C";
            case OrgEnum.M: return "M";
            case OrgEnum.G: return "G";
            case OrgEnum.D: return "D";
            case OrgEnum.W: return "W";
            case OrgEnum.V: return "V";
            case OrgEnum.S: return "S";
            case OrgEnum.Z: return "Z";
            default: return "U";
        }
    }
}

export {OrgEnum};