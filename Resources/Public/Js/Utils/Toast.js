//Plugins.require(Globals.JsPath + "/Utils/CEvent.js");
import CEvent from "./CEvent.js";

    /**
     * Class to View little Toast like Messages to the User.
     */
export default class Toast{
    /**
     * 
     * @param {HTMLElement} template The Template to be used for this Toast.
     */
    constructor(template){

        /**
         * The Template for the Message
         */
        this.template = template;

        /**
         * The Actual instance of the Message
         */
        this.instance = null;

        /**
         * Event wich should be Fired if the Closebutton is Clicked
         */
        this.onCloseButtonClick = new CEvent();
    }

    /**
     * 
     * @param {String} text The Title of this Message
     */
    setTitle(text){
        this.template.querySelector(".mr-auto").innerHTML = text;
    }

    /**
     * 
     * @param {String} body The Body of this Message
     */
    setBody(body){
        this.template.querySelector(".toast-body").innerHTML = body;
    }

    /**
     * 
     * @param {String} header The Title of this Message
     * @param {String} body   The Body of this Message
     */
    set(header, body){
        this.setTitle(header);
        this.setBody(body);
    }

    /**
     * Appends the Toast to the Body of the Website and Shows it to the User.
     */
    show(){
        if(this.instance == null){
            this.instance = document.getElementById("exsv_body").appendChild(this.template);
            jQuery(this.instance).on('hidden.bs.toast', this.onHide.bind(this));
        }
        jQuery(this.instance).toast("show");
    }

    /**
     * Removes the Message from the Document after Hiding it.
     */
    onHide(){
        this.instance.remove();
    }
}
