//Plugins.require(Globals.JsPath + "/Utils/CEvent.js");
import CEvent from "./CEvent.js"

    /**
     * Class for filter functionallity on datasets
     * This Class is meant to be used with a SELECT element
     */
export default class FilterControl{

    static Functions = {
        AlphabetSort : (a,b) => {if(a.value == b.value) return 0; return a.value < b.value ? -1 : 1;},
        IndexCompare : (elem, filter) => {return (elem).toLowerCase().indexOf(filter) != -1;}
    }

    /**
     * 
     * @param {String} name Name of the Filter
     * @param {HTMLElement} control SELECT Element
     * @param {Function} filter Function with implements the filter functionallity
     */
    constructor(name, control, filter){
        /**
         * Name of the Filter
         */
        this.name = name;
        
        /**
         * SELECT/INPUT Element
         */
        this.control = control;
        
        /**
         * Function with implements the filter functionallity
         */
        this.filterFunction = filter; 

        /**
         * Event wich is Called if the Filtercontrolelement changes
         */
        this.onChange = new CEvent();
        this.control.onchange = this.onChange.call.bind(this.onChange);
        this.onChange.add("blurControl", () => this.control.blur());
    }

    /**
     * Applyes the Filter function to every object of the dataset. 
     * @param {Array} dataSets Dataset to apply the filter on
     * @returns {Array} Filtered dataset
     */
    applyFilter(dataSets){
        var that = this;

        if(this.control.selectedOptions[0].getAttribute("data-reset") != undefined) return dataSets;

        var newSet = dataSets.filter((elem) =>{
            return that.filterFunction(elem[this.name], that.control.selectedOptions[0].getAttribute("data-option"));
        });
        return newSet;
    }

    /**
     * 
     * @param {Array} dataset Array of Objects to create the FIlteroptions from
     * @param {Function} func A function (object):[String] to call on every Object from the Dataset 
     */
    createFilterOptionsFromDataset(dataset, func){
        this.clear();
        dataset.forEach((element)=>{
            func(element).forEach((option)=>{
                this.addFilterOption(option);
            });
        });
    }

    /**
     * 
     * @returns Returns the selected HTMLOption Element
     */
    getSelectedOptionElement(){
        return this.control.selectedOptions[0];
    }

    /**
     * Adds a new Option to the Filtercontrol
     * @param {String} value Option to choose from for the user
     * @param {any} caption Caption to use vor the Element
     * @returns {HTMLOptionElement} Resturns the new created Option Element
     */
    addFilterOption(value, caption = ""){
        if(this.control.querySelector("[data-option='" + value + "']") != null) return;

        var elem = document.createElement("option");
        elem.setAttribute("value", value);
        elem.setAttribute("data-option", value);
        elem.innerHTML = caption;
        this.control.appendChild(elem);
        return elem;
    }

    /**
     * Sorts the Dataset with the given function
     * @param {Function} func Function(a,b):bool wich implements the sorting
     */
    sortFilterOptions(func){
        var options = jQuery("#" + this.control.id + " option");
        
        options.sort(function (a,b){
            return func(a,b);
        });

        jQuery(this.control).empty().append(options);
    }

    /**
     * Resets the Filtercontrol to the default value
     */
    reset(){
        this.control.selectedIndex = 0;
    }

    /**
     * Removes all options
     */
    clear(){
        this.control.replaceChildren();
        var clearOption = document.createElement("option");
        clearOption.innerHTML = "---";
        clearOption.setAttribute("data-reset", "true");
        this.control.appendChild(clearOption);
    }

    static createControlGroup(name, control, filter){
        if(control == null){
            control = document.createElement("select");
        }

        control.classList.add("form-control");
        control.setAttribute("style","padding: 0.5rem!important");

        var group = document.createElement("div");
        group.classList.add("input-group");
        group.appendChild(control);

        var append = document.createElement("div");
        append.classList.add("input-group-append");

        var deleteButton = document.createElement("button");
        deleteButton.classList.add("btn");
        deleteButton.classList.add("btn-outline-secondary");
        deleteButton.classList.add("align-self-center");
        deleteButton.classList.add("d-flex");
        deleteButton.classList.add("close");
        deleteButton.type = "button";
        deleteButton.innerHTML = "<span aria-hidden='true'>&times;</span>";
        deleteButton.setAttribute("style", "padding: 0.5rem!important");

        append.appendChild(deleteButton);

        group.appendChild(append);

        var instance = new FilterControl(name, control, filter);

        deleteButton.onclick = () => {
            instance.reset();
            instance.onChange.call();
        };

        return {group: group, instance: instance};
    }
}
