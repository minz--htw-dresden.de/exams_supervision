//Plugins.require(Globals.JsPath + "/Utils/CEvent.js");

import CEvent from "./CEvent.js";

    /**
     * Class to Show Messages and Questions to the User.
     */
export default class Modal{
    /**
     * 
     * @param {HTMLElement} template Template to Create the Modal
     * @param {String} name Name of the Modal
     */
    constructor(template, name){
        this.template = template;
        this.name = name;
        this.instance = null;

        this.onCloseButtonClick = new CEvent();
        this.template.querySelector(".close").onclick = this.onCloseButtonClick.call.bind(this.onCloseButtonClick);
    }

    /**
     * 
     * @param {String} text The Title of this Message
     */
    setTitle(text){
        this.template.querySelector(".modal-title").innerHTML = text;
    }

    /**
     * 
     * @param {String} body The Body of this Message
     */
    setBody(body){
        this.template.querySelector(".modal-body").innerHTML = body;
    }

    /**
     * 
     * @param {String} header The Title of this Message
     * @param {String} body   The Body of this Message
     */
    set(header, body){
        this.setTitle(header);
        this.setBody(body);
    }

    /**
     * 
     * @param {String} caption Caption of the Button
     * @param {Function} callback Function to call if the Button is clicked
     * @param {Boolean} closeOnClick Should the Modal Close itself after the Button was Clicked?
     */
    addButton(caption, callback, closeOnClick = true){
        var b = document.createElement("button");
        b.setAttribute("type", "button");
        b.setAttribute("class", "btn btn-secondary");
        if(closeOnClick){
            b.setAttribute("data-dismiss", "modal");
        }
        b.innerHTML = caption;
        b.onclick = callback;
        this.template.querySelector(".modal-footer").appendChild(b);
    }

    /**
     * Appends the Toast to the Body of the Website and Shows it to the User.
     */
    show(){
        if(this.instance == null){
            this.instance = document.body.appendChild(this.template);
            jQuery(this.instance).on('hidden.bs.modal', this.onHide.bind(this));
        }
        jQuery(this.instance).modal({backdrop: "static"});
    }

    /**
     * Removes the Message from the Document after Hiding it.
     */
    onHide(){
        this.instance.remove();
    }

    /**
     * Creates a simple Modal to ask easy Yes/No questions to the user
     * @param {String} message The question to ask the User
     * @param {Function} onYes Callback to execute on "YES" is clicked
     * @param {Function} onNo Callback to execute on "NO" is clicked
     */
    static ask(message, onYes, onNo){
        var modal = new Modal(Globals.getTemplate("modalDefaultTemplate", 0), "modal_ask_temp");
        modal.set("Aktion ausführen?", message);
        modal.addButton("Ja", onYes, true);
        modal.addButton("Nein", onNo, true);
        modal.onCloseButtonClick.add(onNo);
        modal.show();
    }
}
