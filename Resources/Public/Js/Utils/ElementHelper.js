
export default class ElementHelper{
    static createInput(id, type){
        var input = document.createElement("INPUT");
        input.setAttribute("id", id);
        input.setAttribute("type", type);
        return input;
    }

    static createTableCell(child = null){
        var cell = document.createElement("TD");
        if(child != null){
            cell.appendChild(child);
        }
        return cell;
    }

    static createTableHeaderCell(text = ""){
        var cell = document.createElement("TH");
        cell.appendChild(ElementHelper.createTextElement(text));
        return cell;
    }

    static createTextElement(text = ""){
        var t = document.createElement("TEXT");
        t.innerHTML = text;
        return t;
    }

    static createSupervisionOption(supervisor){
        var option = document.createElement("OPTION");
        option.innerHTML = supervisor.title + " " + supervisor.last_name + ", " + supervisor.first_name;
        option.setAttribute("value", supervisor.id);
        return option;
    }

    static createLoadingIcon(){
        var encaps = document.createElement("div");
        encaps.setAttribute("class", "d-flex justify-content-center");
        var container = document.createElement("div");
        container.classList.add("spinner-border");
        container.setAttribute("role", "status");
        var span = document.createElement("span");
        span.innerHTML="Loading...";
        span.classList.add("sr-only");
        container.appendChild(span);
        encaps.appendChild(container);
        return encaps;
    }

    static createDarkAlert(message){
        var div = document.createElement("div");
        div.classList.add("alert");
        div.classList.add("alert-dark")
        div.setAttribute("role", "alert");
        div.innerHTML = message;
        return div;
    }

    static createPaginationItem(Label){
        var li = document.createElement("li");
        li.classList.add("page-item");
        var a = document.createElement("a");
        a.classList.add("page-link");
        a.href = "#";
        a.innerHTML = Label;
        li.appendChild(a);
        return li;
    }

    static createOptionList(options){
        var result = [];

        options.forEach((elem) =>{
            var op = document.createElement("option");
            op.innerHTML = elem;
            op.value = elem;
            result.push(op);
        });
        return result;
    }
}
