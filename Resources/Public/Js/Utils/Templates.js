
export default class Templates{
    constructor(){
        this.templates = [];
        
        this.loadTemplateFromDom();
    }

    getTemplate(name, index){
        return this.templates[name].cloneNode(true);
    }

    loadTemplateFromDom(){
        var templates = document.querySelectorAll("template");
        templates.forEach((element)=>{
            this.templates[element.id] = element.content.childNodes[0].cloneNode(true);
        });
    }
}