import Modal from "./Utils/Modal.js";

export default class SupervisionAjax{  
    
    static Result = {
        Success : 1,
        Error : 2
    }
    
    static get(data, callback){
        var url = "/?id=" + Globals.PageID + "&type=" + Globals.JsonType;
        jQuery.ajax({
            type: "GET",
            cache: false,
            data: data,
            url: url,
            dataType: "text",
            success: function (data, textStatus, xhr) {
                if(callback != null){
                    try{
                        var ServerAnswer = {
                            Result : SupervisionAjax.Result.Success,
                            Data : JSON.parse(data),
                            Status: xhr.Status,
                            textStatus: textStatus,
                            errorThrown: ""
                        };
                    }catch{
                        var ServerAnswer = {
                            Result : SupervisionAjax.Result.Error,
                            Data : data,
                            Error: {
                                Status: xhr.Status,
                                textStatus: textStatus,
                                errorThrown: data
                            },
                        }
                    }

                    if(xhr.Status >= 400) ServerAnswer.Result = SupervisionAjax.Result.Error;

                    callback(ServerAnswer);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if(callback != null){

                    var ServerAnswer = {
                        Result : SupervisionAjax.Result.Error,
                        Error : {
                            textStatus: textStatus,
                            errorThrown: errorThrown
                        }
                    };

                    callback(ServerAnswer);
                }
            }
        });
    }

    static getPDF(data, callback){
        var url = "/?id=" + Globals.PageID + "&type=" + Globals.PDFType;
        jQuery.ajax({
            type: "GET",
            cache: false,
            data: data,
            url: url,
            dataType: "text",
            success: function (data, textStatus, xhr) {
                if(callback != null){
                    var ServerAnswer = {
                        Result : SupervisionAjax.Result.Success,
                        Data : data,
                        Status: xhr.Status,
                        textStatus: textStatus,
                        errorThrown: ""
                    };

                    if(xhr.Status >= 400) ServerAnswer.Result = SupervisionAjax.Result.Error;

                    callback(ServerAnswer);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if(callback != null){

                    var ServerAnswer = {
                        Result : SupervisionAjax.Result.Error,
                        Error : {
                            textStatus: textStatus,
                            errorThrown: errorThrown,
                            Status: jqXHR.Status
                        }
                    };

                    callback(ServerAnswer);
                }
            }
        });
    }

    static mail(data, callback){
        var url = "/?id=" + Globals.PageID + "&type=" + Globals.MailType;
        jQuery.ajax({
            type: "GET",
            cache: false,
            data: data,
            url: url,
            dataType: "text",
            success: function (data, textStatus, xhr) {
                if(callback != null){
                    var ServerAnswer = {
                        Result : SupervisionAjax.Result.Success,
                        Data : data,
                        Status: xhr.Status,
                        textStatus: textStatus,
                        errorThrown: ""
                    };

                    if(xhr.Status >= 400) ServerAnswer.Result = SupervisionAjax.Result.Error;

                    callback(ServerAnswer);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if(callback != null){

                    var ServerAnswer = {
                        Result : SupervisionAjax.Result.Error,
                        Error : {
                            textStatus: textStatus,
                            errorThrown: errorThrown
                        }
                    };

                    callback(ServerAnswer);
                }
            }
        });
    }

    static showServerErrorMessage(ServerAnswer){
        var modal = new Modal(Globals.getTemplate("modalDefaultTemplate", 0), "ServerErrorModal");
        modal.set("Serverfehler", ServerAnswer.Error.textStatus + "\n" + ServerAnswer.Error.errorThrown);
        //if(ServerAnswer.Status >= 400 && ServerAnswer.Status < 500){
            //modal.onCloseButtonClick.add("", ()=>{window.location.assign("/" + Globals.ErrorPage)});
        //}
        modal.show();
    }
}
