//Plugins.require(Globals.JsPath + "/Handler/TableHandler.js");
import TableHandler from "./TableHandler.js";
import CEvent from "../Utils/CEvent.js";
import ElementHelper from "../Utils/ElementHelper.js";

export class LockedTimesTableHandler extends TableHandler{
    constructor(tableElement){
        super(tableElement);
        this.rowCount = 0;
        this.employeeId = null;
        this.showEmptySetAlert = false;
        this.OnRowsSet.add("onRowsDataSet", this.onRowDataSet.bind(this));
        this.OnChanged = new CEvent();
    }

    setAjaxData(employeeId){
        this.Ajaxdata = { 
            "tx_examssupervision_pi1[emp_id]":employeeId,
            "tx_examssupervision_pi1[action]":"getLockedTimes"
        };
        this.employeeId = employeeId;
    }

    onRowDataSet(){
        this.table.lastChild.appendChild(this.createEmptyRow());
    }

    setRowData(row, data){
        var inputs = row.getElementsByTagName("input");
        row.setAttribute("data-id", data.id);
        inputs[0].value = data.m_date_from;
        inputs[1].value = data.m_date_to;
        inputs[2].value = data.m_time_from;
        inputs[3].value = data.m_time_to;
        if(data.m_all_day == "1")
        inputs[4].setAttribute("checked", true);
        return 0;
    }

    onInputChanged(event){
        var row = event.target.closest("tr");
        var inputs = row.getElementsByTagName("input");
        if(this.isValid(inputs)){
            this.table.lastChild.appendChild(this.createEmptyRow());
        }
        console.log("t");
        if(event.target.value != "")
            this.OnChanged.call();
    }

    createEmptyRow(){
        var that = this;
        var row = document.createElement("tr");
        row.setAttribute("id", "new_" + this.rowCount++);
        
        row.appendChild(ElementHelper.createTableCell(ElementHelper.createInput("i_" + this.rowCount + "_date_from", "date")));
        row.appendChild(ElementHelper.createTableCell(ElementHelper.createInput("i_" + this.rowCount + "_date_to", "date")));
        row.appendChild(ElementHelper.createTableCell(ElementHelper.createInput("i_" + this.rowCount + "_time_from", "time")));
        row.appendChild(ElementHelper.createTableCell(ElementHelper.createInput("i_" + this.rowCount + "_time_to", "time")));
        row.appendChild(ElementHelper.createTableCell(ElementHelper.createInput("i_" + this.rowCount + "_day", "checkbox")));
        row.appendChild(ElementHelper.createTableCell(null));

        var inputs = row.getElementsByTagName("input");
        for(var i = 0; i < inputs.length; i++) inputs[i].onblur = (event) => {that.onInputChanged(event);};
        return row;
    }

    isValid(inputs){
        if(inputs[0].value == "") return false;
        if((inputs[2].value == "" || inputs[3].value == "") && inputs[4].checked == false) return false;

        if(inputs[1].value == "")
            inputs[1].value = inputs[0].value;

        if(inputs[4].checked){
            inputs[2].value = "00:01";
            inputs[3].value = "23:59";
        }
        return true;
    }

    isEmpty(inputs){
        var result = true;
        for(var i = 0; i < inputs.length-1; i++){
            result &= inputs[i].value == "";
        }
        return result;
    }

    extractLockedTimesFromTable(){
        var result = [];
        var rows = this.table.getElementsByTagName("tr");

        for(var i = 0; i < rows.length; i++){
            var inputs = rows[i].getElementsByTagName("input");
            //no inputs in row (e.g header)
            if(inputs.length == 0) continue;

            if(this.isEmpty(inputs)) continue;
            if(!this.isValid(inputs)){
                for(var i = 0; i < inputs.length; i++)
                    inputs[i].style.borderColor = "red";
                
                continue;
            }
            var set = {
                id : rows[i].getAttribute("data-id"),
                m_pers_id   : this.employeeId,
                m_date_from : inputs[0].value,
                m_date_to   : inputs[1].value,
                m_time_from : inputs[2].value,
                m_time_to   : inputs[3].value,
                m_all_day   : inputs[4].checked == true ? "1" : "0"
            }
            result.push(set);
        }
        return result;
    }
}
