import { PaginatedTable } from "./PaginatedTable.js";
import FilterControl from "../Utils/FilterControl.js";

export default class SupervisorTableHandler extends PaginatedTable{
    constructor(table, paginationArea){
        super(table, paginationArea);
        this.paginationType = "Str";
        this.paginationProperty = "Name";

        this.makeFilterable(document.getElementById("supervisor_name"), "Name", FilterControl.Functions.IndexCompare);

        this.OnRowsSet.add("Supervisor_RowsSet", this.onRowsSetCallback.bind(this));

        this.makeSortable(document.getElementById("supervisor_title"), "Titel");
        this.makeSortable(document.getElementById("supervisor_name"), "Name");
        this.makeSortable(document.getElementById("supervisor_prio"), "Prioritaet");
        this.makeSortable(document.getElementById("supervisor_count"), "Anz");
    }

    onRowsSetCallback(){
        this.getFilter("Name")[0].createFilterOptionsFromDataset(this.data, (a)=>{return [a.Name];});
    }

    setAjaxData(fakID){
        this.Ajaxdata = { 
            "tx_examssupervision_pi1[fak_id]":fakID,
            "tx_examssupervision_pi1[action]":"getSupervisorStats"
        };
    }

    makeSortable(headerCell, dataName){
        if(headerCell.getAttribute("data-sortProperty") == null){
            headerCell.setAttribute("data-sortProperty", dataName);
        }

        var caption = headerCell.querySelector("[aria-description='caption']");
        if(caption == null){
            caption = document.createElement("span");
            caption.setAttribute("aria-description", "caption");
            caption.innerHTML = headerCell.innerHTML;
            headerCell.replaceChildren();
            headerCell.appendChild(caption);
        }

        headerCell.classList.add("table_header_sortable");
        caption.onclick = ((event) => {
            this.sortTableRows(event.target.closest("th"));    
        }).bind(this);
        headerCell.onclick = ((event) => {
            if(event.target.closest("th").getAttribute("data-sortProperty") != this.paginationProperty){
                this.paginationType = "Num";
            }else{
                this.paginationType = "Str";
            }
            this.setPaginationNav();
        }).bind(this);
    }

    setRowData(row, data){
        var tds = row.querySelectorAll("td");
        row.setAttribute("data-id", data.ID);
        tds[0].innerHTML = data.Titel;
        tds[1].innerHTML = data.Name;
        tds[2].querySelector("select").value = data.Prioritaet;
        tds[3].innerHTML = data.Anz;
        return 0;
    }  
}
