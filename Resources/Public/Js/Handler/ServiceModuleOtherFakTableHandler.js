import TableHandler from "./TableHandler.js";
import { OrgEnum } from "../Utils/DataEnums.js";

export default class ServiceModuleOtherFakTableHandler extends TableHandler{

    constructor(table){
        super(table);
        this.numPendingSupervisors = 0;
        this.OnRowsSet.add("rowsSet_ServiceModuleOwn", this.onRowsSet.bind(this));
    }

    setAjaxData(fakID){
        this.Ajaxdata = { 
            "tx_examssupervision_pi1[fak_id]":fakID,
            "tx_examssupervision_pi1[action]":"getOtherServiceModulesForFaculty"
        };
    }

    onRowsSet(){
        var navElement = document.getElementById("v-pills-nav-servicemodule-tab");
        var span = navElement.querySelector("span");
        if(this.numPendingSupervisors == 0){
            if(span == null) return;
            navElement.removeChild(span);
        }
        if(span == null)
            navElement.innerHTML = navElement.innerHTML + "<span class='badge badge-primary'>&nbsp;" + this.numPendingSupervisors + "&nbsp;</span>";
        else
            span.innerHTML = this.numPendingSupervisors;

        this.numPendingSupervisors = 0;
    }

    setRowData(row, data){
        var tds = row.querySelectorAll("td");
        //var service = 1;
        row.setAttribute("data-id", data.id);

        tds[3].innerHTML = data.datum;
        tds[4].innerHTML = data.zeit_von.slice(0, -3);
        tds[5].innerHTML = data.zeit_bis.slice(0, -3);
        var examiner = JSON.parse(data.Examiner);
        var examiner_names = [];
        examiner.forEach(element => {
            if(element.first_name != null)
                examiner_names.push(element.last_name + ", " + element.first_name.slice(0,1) + ".");
            //if(element.faculty != data.faculty) service = element.faculty;
        });
        tds[6].innerHTML = examiner_names.join("<br>");
        tds[2].innerHTML = data.modul;
        tds[7].innerHTML = data.raum;

        let badge = tds[0].querySelector("span");
            badge.classList.remove("badge-danger");
            badge.classList.remove("badge-warning");
            badge.classList.remove("badge-success");
            switch(data.status){
                case 0:
                    badge.classList.add("badge-danger");
                    break;
                case 1:
                    badge.classList.add("badge-warning");
                    break;
                case 2:
                case 3:
                    badge.classList.add("badge-success");
            }
            
        if(data.hasSharedSupervisors == true){
            this.numPendingSupervisors++;
            tds[1].innerHTML = OrgEnum.IdToStr(data.faculty) + "<span class='badge badge-primary'>&nbsp;!&nbsp;</span>";
            return 1;
        }
        else
            tds[1].innerHTML = OrgEnum.IdToStr(data.faculty);

        return 0;
    }
}