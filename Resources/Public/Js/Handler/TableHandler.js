//Plugins.require(Globals.JsPath + "/Utils/ElementHelper.js");
//Plugins.require(Globals.JsPath + "/Utils/CEvent.js");
//Plugins.require(Globals.JsPath + "/Utils/FilterControl.js");
//Plugins.require(Globals.JsPath + "/Utils/InputFilterControl.js");

import ElementHelper from "../Utils/ElementHelper.js";
import CEvent from "../Utils/CEvent.js";
import FilterControl from "../Utils/FilterControl.js";
import InputFilterControl from "../Utils/InputFilterControl.js";
import SupervisionAjax from "../SupervisionAjax.js";

/**
 * Base Class for all Tables on the Website
 */
export default class TableHandler{
    /**
     * 
     * @param {HTMLElement} table the Table to be handled
     */
    constructor(table){
        /**
         * The Table wich should be Handled 
         */
        this.table = table;

        /**
         * A Table wich serves as Header Row
         */
        //this.tableHeader = null;

        /**
         * Array of Templates used by the handler
         */
        this.templates = [];

        /**
         * Array of FilterControls to implement filtering the dataSets
         * @see FilterControl
         */
        this.filter = [];

        /**
         * Extra Header Row to store Filter input elements
         */
        this.filterHeaderRow = null;

        /**
         * The Data to be shown in this Table
         */
        this.data = [];

        /**
         * The raw, unfiltered Dataset to restore Data after filtering
         */
        this.dataUnfiltered = [];

        /**
         * Data to send to the Server
         */
        this.Ajaxdata = "";

        /**
         * Determines if a Table should show a special Row to hint the user that no Data can be shown
         */
        this.showEmptySetAlert = true;

        /**
         * Count of the Headercells
         */
        this.headerCellCount = 0;

        /**
         * Event to be called after all rows are set
         */
        this.OnRowsSet = new CEvent();

        /**
         * Did we activate FloatingHead?
         */
        this.floatingHead = false;

        this.OnTableRowClick = new CEvent();
    }

    /**
     * 
     * @param {String} dataName Name of the Property wich should be filtered
     * @param {HTMLElement} control The Element on the Website
     * @param {Function} filter A function wich implements filter logic
     */
    addFilterControl(dataName, control, filter){
        var instance = null;

        switch(control.localName){
            case "select" :
                var instance = new FilterControl(dataName, control, filter);
                instance.onChange.add(dataName + "filter_change", this.onFilterChange.bind(this));
                break;
            case "input":
                var instance = new InputFilterControl(dataName, control, filter);
                instance.onChange.add(dataName + "filter_change", this.onFilterChange.bind(this));
        }
        
        this.filter.push(instance);
    }

    addFilterControlInstance(dataName, instance){
        instance.onChange.add(dataName + "filter_change", this.onFilterChange.bind(this));
        this.filter.push(instance);
    }

    /**
     * 
     * @param {String} name Name of the Property
     * @returns Returns the Filter as FilterControl if it was found, null otherwise
     * @see {FilterControl}
     */
    getFilter(name){
        return this.filter.filter(elem => elem.name == name);
    }

    /**
     * 
     * @param {String} name The Name of the Property to check if there is a Filter for
     * @returns True if such a Filter was found, false otherwise.
     */
    hasFilter(name){
        return (this.filter.filter(elem => elem.name == name)).length != 0;
    }

    /**
     * Helper function to add a TBODY to the Table
     */
    addtBodyElement(){
        if(this.table.querySelector("tbody") == null)
            this.table.appendChild(document.createElement("tbody"));
    }

    /**
     * 
     * @param {String} property The property wich should be extracted
     * @returns A Array of all Values for this Property present in the Dataset
     */
    createListFromDataProperty(property){
        var array = [];
        this.data.forEach((elem) => {
            array.push(elem[property]);
        });
        return [...new Set(array)];
    }

    /**
     * 
     * @param {String} name Name of the Template
     * @param {HTMLCollection} template The Template as Collection
     */
    addTemplate(name, template){
        this.templates[name] = template;
    }

    /**
     * 
     * @param {FilterControl} filter 
     * @param {*} option 
     */
    addFilterOption(filter, option){
        this.filter.forEach(filterControl =>{
            if(filterControl.name == filter){
                filterControl.addFilterOption(option);
            }
        })
    }

    /**
     * 
     * @param {HTMLElement} headerElement The Table Header Element
     */
    /*setTableHeader(headerElement){
        this.tableHeader = headerElement;
    }*/

    /**
     * Clears all Filters for this table
     */
    clearFilter(){
        this.filter.forEach(filterControl =>{
            var val = filterControl.control.value;
            filterControl.clear();
            filterControl.control.value = val;
        })
    }

    /**
     * Deletes all Data from the Table
     */
    clearTable(){
        var that = this;
        if(this.table.querySelector("tbody") != null)
            this.table.querySelector("tbody").replaceChildren();
    }

    /**
     * Resets all Filters to a Default value
     */
    resetFilter(){
        this.filter.forEach(function(elem){
            elem.reset();
        });
        this.data = this.dataUnfiltered;
        this.setRows();
    }

    /**
     * Applys all filters to the datasets
     */
    applyFilter(){
        var filteredData = this.dataUnfiltered;
        var that = this;

        this.filter.forEach(filterControl =>{
            filteredData = filterControl.applyFilter(filteredData);
        });

        this.data = filteredData;
        this.setRows();
    }

    /**
     * Proxy function for OnChange Events of FilterControls
     * @param {Event} e Event given from the FilterControl
     */
    onFilterChange(e){
        this.applyFilter();
    }

    /**
     * Sets the initial Datasets for this table
     * @param {Array} data Array of Objects containing the Table data
     */
    setDataSets(data){
        if((typeof data === 'string')){
            data = JSON.parse(data);
        }
        this.data = data;
        this.dataUnfiltered = data;
    }

    /**
     * Creates all Table Rows for this.data
     */
    setRows(){
        this.clearTable();
        this.clearFilter();
        var that = this;

        if(this.data.length == 0 && this.showEmptySetAlert){
            var headerlength = this.getHeaderCellCount();
            var div = ElementHelper.createDarkAlert("Keine Daten zum Anzeigen vorhanden!");
            var tr = document.createElement("tr");
            var td = document.createElement("td");
            tr.appendChild(td);
            td.setAttribute("colspan", headerlength);
            td.appendChild(div);
            that.table.querySelector("tbody").appendChild(tr);
        }

        this.data.forEach(set => {
            if(this.templates["row"] == undefined) console.log(Globals);
            var row = this.templates["row"].cloneNode(true);
            if(row.tagName != "TR")
                row.querySelector("tr").onclick = function(e){that.OnTableRowClick.call(e);};
            else
                row.onclick = function(e){that.OnTableRowClick.call(e);};
            
            if(that.setRowData(row, set) != 1)
                that.table.querySelector("tbody").appendChild(row);
            else
                that.table.querySelector("tbody").prepend(row);
        });
        this.OnRowsSet.call(this);
        if(this.floatingHead) jQuery(this.table).floatThead("reflow");
    }

    /**
     * 
     * @param {HTMLElement} row The Row element
     * @param {Object} set The data object for this row
     */
    setRowData(row, set){}

    /**
     * Special function to show only the Datasets from startIndex to endIndex
     * @param {Int} from startIndex
     * @param {Int} to endIndex
     */
    showDataIndices(from, to){
        var trs = this.table.querySelectorAll("tr");
        for(var i = 0; i < trs.length; i++){
            if(i <= from || i > to)
                jQuery(trs[i]).hide();
            else
                jQuery(trs[i]).show();
        }
    }

    /**
     * 
     * @returns The number of Headercells for this table.
     */
    getHeaderCellCount(){
        return this.table.querySelectorAll("th").length;
    }

    setFloatingThead(){
        jQuery(this.table).floatThead({
            scrollContainer: function($table){
                return $table.closest(".exams_table");
            },
            position: 'auto',
            autoReflow: true
        });
        this.floatingHead = true;
    }

    addFilterHeaderRow(){
        var tr = document.createElement("tr");
        tr.classList.add("tableFilterHeader")
        for(var i = 0; i < this.getHeaderCellCount(); i++){
            let th = document.createElement("th");
            tr.appendChild(th);
        }
        this.table.querySelector("thead").appendChild(tr);
        this.filterHeaderRow = tr;
    }

    makeFilterable(headerCell, dataName, filter, controlType = "input"){

        if(this.filterHeaderRow == null) this.addFilterHeaderRow();
        var span = document.createElement("span");
        var caption = document.createElement("span");
        caption.setAttribute("aria-description", "caption");
        caption.innerHTML = headerCell.innerHTML;
        span.innerHTML = " &#128269;";
        span.onclick = this.onFilterHeaderCellClick.bind(this);
        headerCell.setAttribute("data-filter", dataName);
        headerCell.replaceChildren();
        headerCell.appendChild(caption);
        headerCell.appendChild(span);

        var control = document.createElement(controlType);
        control.id = dataName + "filter_auto" + Date.now();

        switch(controlType){
            case "select":
                var group = FilterControl.createControlGroup(dataName, control, filter);
                break;
            case "input":
                var group = InputFilterControl.createControlGroup(dataName, control, filter);
                break;
        }
        
        this.filterHeaderRow.children[headerCell.cellIndex].append(group.group);
        this.addFilterControlInstance(dataName, group.instance);
    }

    makeSortable(headerCell, dataName){
        if(headerCell.getAttribute("data-sortProperty") == null){
            headerCell.setAttribute("data-sortProperty", dataName);
        }

        var caption = headerCell.querySelector("[aria-description='caption']");
        if(caption == null){
            caption = document.createElement("span");
            caption.setAttribute("aria-description", "caption");
            caption.innerHTML = headerCell.innerHTML;
            headerCell.replaceChildren();
            headerCell.appendChild(caption);
        }

        headerCell.classList.add("table_header_sortable");
        caption.onclick = ((event) => {
            this.sortTableRows(event.target.closest("th"));    
        }).bind(this);        
        
    }

    onFilterHeaderCellClick(event){
        var th = event.target.closest("th");
        this.filterHeaderRow.classList.add("tableFilterHeaderExpanded");
        if(this.floatingHead) jQuery(this.table).floatThead("reflow");
    }

    /**
     * Requests the table data from the Server and resets the table.
     */
    getTableData(){
        var that = this;
        SupervisionAjax.get(this.Ajaxdata, (data) => {
            if(data.Result === SupervisionAjax.Result.Success){
                (that.setDataSets.bind(that, data.Data))();
                (that.setRows.bind(that))();
            }else{
                SupervisionAjax.showServerErrorMessage(data);
            }
        });
        this.clearTable();
        this.clearFilter();
        this.setLoadingIcon();
    }

    sortTableRows(rowHeaderElement){
        var th = rowHeaderElement;
        var func = null;

        rowHeaderElement.closest("table").querySelectorAll("th").forEach((elem) =>{
            if(elem != rowHeaderElement)
                elem.removeAttribute("data-sort");
        });

        var property = th.getAttribute("data-sortProperty");
        if(th.getAttribute("data-sort") == null){
            th.setAttribute("data-sort", "desc");
        }else{
            th.setAttribute("data-sort", (th.getAttribute("data-sort") == "desc" ? "asc" : "desc")); 
        }

        if(th.getAttribute("data-sort") == "desc"){
            func = (a,b) =>{
                if(a == b) return 0;
                return a < b ? -1 : 1;
            }
        }else{
            func = (a,b) =>{
                if(a == b) return 0;
                return a < b ? 1 : -1;
            }
        }

        this.data.sort((a,b) =>{
            var valueA = a[property];
            var valueB = b[property];
            return func(valueA, valueB);
        });

        this.setRows();
        if(this.floatingHead) jQuery(this.table).floatThead("reflow");
    }

    /**
     * Creates a Loading Icon for the table.
     */
    setLoadingIcon(){
        var td = document.createElement("td");
        td.appendChild(ElementHelper.createLoadingIcon());
        td.setAttribute("colspan", this.getHeaderCellCount());
        this.table.querySelector("tbody").appendChild(td);
    }

    /**
     * 
     * @param {Object} arr Object to serialize into an Action String
     * @returns The Action String for Typo3
     */
    getAction(arr){
        var res = Object();
        var iterator = Object.keys(arr);
        for(const key of iterator){
            res["tx_examssupervision_pi1[" + key + "]"] = arr[key];
        }
        return res;
    }
}
