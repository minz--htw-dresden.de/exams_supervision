//Plugins.require(Globals.JsPath + "/Handler/PaginatedTable.js");
import { PaginatedTable } from "./PaginatedTable.js";
import FilterControl from "../Utils/FilterControl.js";

    export class ExamsReportTableHandler extends PaginatedTable{
        constructor(table, paginationArea){
           super(table, paginationArea);

            this.dateFilter = null;
            this.examinerFilter = null;
            this.examsInfo = null;
            this.selected = null;
            this.addtBodyElement();

            this.makeFilterable(document.getElementById("examReport_header_modul"), "modul", FilterControl.Functions.IndexCompare);
            this.makeFilterable(document.getElementById("examReport_header_date"), "datum", FilterControl.Functions.IndexCompare);
            this.makeFilterable(document.getElementById("examReport_header_room"), "raum", FilterControl.Functions.IndexCompare);
            this.makeFilterable(document.getElementById("examReport_header_examiner"), "Examiner", (elem, filter) =>{
                var examiner = JSON.parse(elem);
                var result = false;
                examiner.forEach((ex) => {
                    result |= (ex.last_name).toLowerCase().indexOf(filter.toLowerCase().split(",")[0]) != -1;
                });
                return result;
            });

            this.OnRowsSet.add("Exams_repost_RowsSet", this.onRowsSetCallback.bind(this));

            this.makeSortable(document.getElementById("examReport_header_modul"), "modul");
            this.makeSortable(document.getElementById("examReport_header_date"), "datum");
            this.makeSortable(document.getElementById("examReport_header_examiner"), "Examiner");
            this.makeSortable(document.getElementById("examReport_header_room"), "raum");
            this.makeSortable(document.getElementById("examReport_header_start"), "zeit_von");
            this.makeSortable(document.getElementById("examReport_header_end"), "zeit_bis");
        }

        onRowsSetCallback(){
            this.getFilter("modul")[0].createFilterOptionsFromDataset(this.data, (a) =>{return [a.modul];});
            this.getFilter("datum")[0].createFilterOptionsFromDataset(this.data, (a)=>{return [a.datum];});
            this.getFilter("raum")[0].createFilterOptionsFromDataset(this.data, (a)=>{
                return a.raum.split("/").map((element)=>{
                    if(element[0] == " ") element = element.substring(1);
                    if(element[element.length-1] == " ") element = element.substring(0, element.length-1);
                    return element;
                });
            });
            this.getFilter("Examiner")[0].createFilterOptionsFromDataset(this.data, (a)=>{
                var examiner = JSON.parse(a.Examiner);
                var examiner_names = [];
                examiner.forEach(element => {
                    examiner_names.push(element.last_name);
                });
                return examiner_names;
            });

            this.getFilter("Examiner")[0].sortFilterOptions(FilterControl.Functions.AlphabetSort);
            this.getFilter("modul")[0].sortFilterOptions(FilterControl.Functions.AlphabetSort);
            this.getFilter("raum")[0].sortFilterOptions(FilterControl.Functions.AlphabetSort);
        }

        setAjaxData(fakID){
            this.Ajaxdata = { 
                "tx_examssupervision_pi1[fak_id]":fakID,
                "tx_examssupervision_pi1[action]":"getExamsForFaculty"
            };
        }

        setRowData(row, data){
            var tds = row.getElementsByTagName("td");
            row.setAttribute("data-id", data.id);
            row.setAttribute("date", data.datum);
            row.setAttribute("data-length", data.dauer);
            tds[0].innerHTML = data.modul;
            tds[1].innerHTML = data.datum;
            tds[2].innerHTML = data.zeit_von.slice(0, -3);
            tds[3].innerHTML = data.zeit_bis.slice(0, -3);
            tds[5].innerHTML = data.raum;

            var examiner = JSON.parse(data.Examiner);
            var examiner_names = [];
            examiner.forEach(element => {
                examiner_names.push(element.last_name);
            });
            tds[4].innerHTML = examiner_names.join("/");
            return 0;
        }
    }
