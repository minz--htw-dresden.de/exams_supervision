
//Plugins.require(Globals.JsPath + "/Handler/TableHandler.js");
import TableHandler from "./TableHandler.js";

export class SupervisionsTableHandler extends TableHandler{
    constructor(table){
        super(table);
    }

    setAjaxData(empId){
        this.Ajaxdata = { 
            "tx_examssupervision_pi1[emp_id]":empId,
            "tx_examssupervision_pi1[action]":"getPlannedSupervisionsForEmployee"
        };
    }

    setRowData(row, data){
        this.data[data.id] = data;
        var tds = row.getElementsByTagName("td");
        tds[0].innerHTML = data.modul;
        tds[1].innerHTML = data.datum;
        tds[2].innerHTML = data.zeit_von.slice(0, -3);
        tds[3].innerHTML = data.zeit_bis.slice(0, -3);
        tds[5].innerHTML = data.raum;

        var examiner = JSON.parse(data.Examiner);
        var examiner_names = [];
        examiner.forEach(element => {
            examiner_names.push(element.last_name);
        });
        tds[4].innerHTML = examiner_names.join("/");
        return 0;
    }
}