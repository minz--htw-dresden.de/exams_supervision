import TableHandler from "./TableHandler.js";
import { OrgEnum } from "../Utils/DataEnums.js";

export default class ServiceModuleOwnFakTableHandler extends TableHandler{

    constructor(table){
        super(table);
    }

    setAjaxData(fakID){
        this.Ajaxdata = { 
            "tx_examssupervision_pi1[fak_id]":fakID,
            "tx_examssupervision_pi1[action]":"getOwnServiceModulesForFaculty"
        };
    }

    setRowData(row, data){
        var tds = row.querySelectorAll("td");
        //var service = 1;
        row.setAttribute("data-id", data.id);
        
        tds[1].innerHTML = data.modul;
        tds[2].innerHTML = data.datum;
        tds[3].innerHTML = data.zeit_von.slice(0, -3);
        tds[4].innerHTML = data.zeit_bis.slice(0, -3);
        var examiner = JSON.parse(data.Examiner);
        var examiner_names = [];
        examiner.forEach(element => {
            if(element.first_name != null)
                examiner_names.push(element.last_name + ", " + element.first_name.slice(0,1) + ".");
            //if(element.faculty != data.faculty) service = element.faculty;
        });
        tds[5].innerHTML = examiner_names.join("<br>");
        tds[0].innerHTML = OrgEnum.IdToStr(data.planning_faculty);
        return 0;
    }
}