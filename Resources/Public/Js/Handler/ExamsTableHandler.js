//Plugins.require(Globals.JsPath + "/Handler/TableHandler.js");
import TableHandler from "./TableHandler.js";
import FilterControl from "../Utils/FilterControl.js";

    export class ExamsTableHandler extends TableHandler{
        constructor(table, praefix){
           super(table);
            this.praefix = praefix;
            this.dateFilter = null;
            this.examinerFilter = null;
            this.examsInfo = null;
            this.selected = null;
            this.addtBodyElement();
            
            this.OnRowsSet.add("ExamsRowsSet_Sort", this.onRowsSetCallback.bind(this));
            
            this.makeFilterable(document.getElementById(this.praefix + "_header_Datum"), "datum", FilterControl.Functions.IndexCompare);
            this.makeFilterable(document.getElementById(this.praefix + "_header_Modul"), "modul", FilterControl.Functions.IndexCompare);
            this.makeFilterable(document.getElementById(this.praefix + "_header_Räume"), "raum", FilterControl.Functions.IndexCompare);

            this.makeFilterable(document.getElementById(this.praefix + "_header_Prüfer"), "Examiner", (elem, filter) =>{
                var examiner = JSON.parse(elem);
                var result = false;
                examiner.forEach((ex) => {
                    result |= (ex.last_name).toLowerCase().indexOf(filter.toLowerCase().split(",")[0]) != -1;
                });
                return result;
            });

            this.makeFilterable(document.getElementById(this.praefix + "_header_Status"), "status", (elem, filter) => {
                return (elem == 3 ? 2 : elem) == filter;
            }, "select");

            this.makeSortable(document.getElementById(this.praefix + "_header_Modul"), "modul");
            this.makeSortable(document.getElementById(this.praefix + "_header_Datum"), "datum");
            this.makeSortable(document.getElementById(this.praefix + "_header_Prüfer"), "Examiner");
            this.makeSortable(document.getElementById(this.praefix + "_header_Räume"), "raum");
            this.makeSortable(document.getElementById(this.praefix + "_header_Beginn"), "zeit_von");
            this.makeSortable(document.getElementById(this.praefix + "_header_Ende"), "zeit_bis");
        }

        onRowsSetCallback(){
            this.getFilter("modul")[0].createFilterOptionsFromDataset(this.data, (a) =>{return [a.modul];});
            this.getFilter("datum")[0].createFilterOptionsFromDataset(this.data, (a)=>{return [a.datum];});
            this.getFilter("raum")[0].createFilterOptionsFromDataset(this.data, (a)=>{
                return a.raum.split("/").map((element)=>{
                    if(element[0] == " ") element = element.substring(1);
                    if(element[element.length-1] == " ") element = element.substring(0, element.length-1);
                    return element;
                });
            });
            this.getFilter("Examiner")[0].createFilterOptionsFromDataset(this.data, (a)=>{
                var examiner = JSON.parse(a.Examiner);
                var examiner_names = [];
                examiner.forEach(element => {
                    examiner_names.push(element.last_name + ", " + element.first_name);
                });
                return examiner_names;
            });

            var op = this.getFilter("status")[0].addFilterOption("0", "Ungeplant");
            //op.classList.add("badge-danger");
            op = this.getFilter("status")[0].addFilterOption("1", "Teilgeplant");
            //op.classList.add("badge-warning");
            op = this.getFilter("status")[0].addFilterOption("2", "Geplant");
            //op.classList.add("badge-success");

            this.getFilter("Examiner")[0].sortFilterOptions(FilterControl.Functions.AlphabetSort);
            this.getFilter("modul")[0].sortFilterOptions(FilterControl.Functions.AlphabetSort);
            this.getFilter("raum")[0].sortFilterOptions(FilterControl.Functions.AlphabetSort);
            this.setFloatingThead();
        }

        setAjaxData(fakID){
            this.Ajaxdata = { 
                "tx_examssupervision_pi1[fak_id]":fakID,
                "tx_examssupervision_pi1[action]":"getExamsForFaculty"
            };
        }

        setExamInfo(id){
            var exam = this.data.find(element => element.id == id);
            if(exam != null)
                this.selected = exam;
            else{
                exam = {datum: "--", dauer: "--", art: "--", status: 0};
            }
            this.examsInfo.querySelector("[id=data-date]").innerHTML = exam.datum;
            this.examsInfo.querySelector("[id=data-length]").innerHTML = exam.dauer;
            this.examsInfo.querySelector("[id=data-type]").innerHTML = exam.art;
            document.querySelector("[id=data-state]").checked = exam.status == 3;
        }

        setRowData(row, data){
            var tds = row.getElementsByTagName("td");
            row.setAttribute("data-id", data.id);
            row.setAttribute("date", data.datum);
            row.setAttribute("data-length", data.dauer);
            tds[1].innerHTML = data.modul;
            tds[2].innerHTML = data.datum;
            tds[3].innerHTML = data.zeit_von.slice(0, -3);
            tds[4].innerHTML = data.zeit_bis.slice(0, -3);
            tds[6].innerHTML = data.raum;
            
            let badge = tds[0].querySelector("span");
            badge.classList.remove("badge-danger");
            badge.classList.remove("badge-warning");
            badge.classList.remove("badge-success");
            switch(data.status){
                case 0:
                    badge.classList.add("badge-danger");
                    break;
                case 1:
                    badge.classList.add("badge-warning");
                    break;
                case 2:
                case 3:
                    badge.classList.add("badge-success");
            }

            var examiner = JSON.parse(data.Examiner);
            var examiner_names = [];
            examiner.forEach(element => {
                if(element.first_name != null)
                    examiner_names.push(element.last_name + ", " + element.first_name.slice(0,1) + ".");
            });
            tds[5].innerHTML = examiner_names.join("<br>");
            return 0;
        }
    }
