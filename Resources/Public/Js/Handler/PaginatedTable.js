import TableHandler from "./TableHandler.js";
import ElementHelper from "../Utils/ElementHelper.js";

    /**
     * This Class creates a Table for large Datasets.
     * The Table is splitted into viewable Pages.
     * @extends TableHandler
     */
    export class PaginatedTable extends TableHandler{
        /**
         * 
         * @param {HTMLElement} table Table element
         * @param {HTMLElement} paginationArea Element for the Pagination elements
         */
        constructor(table, paginationArea){
            super(table);

            /**
             * Initial size of Datasets to show per table
             */
            this.pageSize = 10;

            /**
             * The Available PageSizes for this table.
             */
            this.availPageSizes = ["10", "20", "50", "Alle"];
            /**
             * Count of current page
             */
            this.page = 1;
            this.paginationArea = paginationArea;
            this.paginationProperty = "Name";

            /**
             * The type of the Pagination Caption
             */
            this.paginationType = "Num";
            if(paginationArea == undefined){
                console.warn("Pagination Area is undefined.");
            }
            this.setPageSizeSelect();
        }

        /**
         * Sets the page to show
         * @param {Int} page The page to show
         */
        showPage(page){
            this.page = page;
            this.setRows();
        }

        /**
         * 
         * @param {Event} e Event from the pagination item wich has been clicked
         */
        onPaginationItemClick(e){
            e.preventDefault();
            var page = 0;
            switch(this.paginationType){
                case "Num":
                    page = e.target.innerHTML;
                    break;
                case "Str":
                    page = e.target.getAttribute("data-page");
                    break;
            }
            this.showPage(page);
        }

        /**
         * Creates the pagination nav items.
         * @returns This function returns nothing.
         */
        setPaginationNav(){
            if(this.paginationArea == null) return;

            var ul = this.paginationArea.querySelector("ul");
            var caption = "";
            ul.replaceChildren();
            for(var i = 0, p = 1; i < this.data.length; i+=this.pageSize, p++){
                switch(this.paginationType){
                    case "Num":
                        caption = p;
                        break;
                    case "Str":
                        var endindex = i+this.pageSize >= this.data.length ? this.data.length-1 : i+this.pageSize-1;
                        caption = this.data[i][this.paginationProperty].slice(0,2) + "--" + this.data[endindex][this.paginationProperty].slice(0,2);
                        break;
                }
                var li = ElementHelper.createPaginationItem(caption);
                li.querySelector("a").setAttribute("data-page", p);
                li.querySelector("a").onclick = this.onPaginationItemClick.bind(this);
                ul.appendChild(li);
            }
        }

        setPageSizeSelect(){
            var that = this;
            var select = this.paginationArea.querySelector("select");
            if(select == null) return;
            select.replaceChildren();
            this.availPageSizes.forEach((element) =>{
                var option = document.createElement("option");
                option.innerHTML = element;
                select.appendChild(option);
            });
            select.onchange = (event)=>{
                console.log(event);
                that.pageSize = event.target.value != "Alle" ? Number.parseInt(event.target.value) : Number.MAX_SAFE_INTEGER;
                that.showPage(1);
            };
        }

        /**
         * Creates all Table Rows for this.data
         */
        setRows(){
            this.clearTable();
            var that = this;
    
            if(this.data.length == 0 && this.showEmptySetAlert){
                var div = ElementHelper.createDarkAlert("Keine Daten zum Anzeigen vorhanden!");
                that.table.lastChild.appendChild(div);
            }

            for(var i = (this.page-1)*this.pageSize; i < this.page*this.pageSize && i<this.data.length; i++){
                if(this.templates["row"] == undefined) console.log(Globals);
                var row = this.templates["row"].cloneNode(true);
                that.table.lastChild.appendChild(row);
                that.setRowData(row, this.data[i]);
            }

            this.OnRowsSet.call();
            //this.styleHeader();
            this.setPaginationNav();
        }
    }
