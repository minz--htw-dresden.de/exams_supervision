//Plugins.require(Globals.JsPath + "/View.js");
//Plugins.require(Globals.JsPath + "/Utils/Toast.js");

import View from "../View.js";
import ElementHelper from "../Utils/ElementHelper.js";
import SupervisionAjax from "../SupervisionAjax.js";

    export class FacultyReportView extends View{
        constructor(){
            super();
            this.AjaxData = null;

            this.report_exam_count_container = document.getElementById("report_exam_count");
            this.report_exam_no_sup_count_container = document.getElementById("report_exam_no_sup_count");

            this.registerActivator(document.getElementById("v-pills-nav-berichte-tab"));
            this.onBecomeActive.add("faculty_report_update", this.update.bind(this));
        }

        update(){
            this.AjaxData = { 
                "tx_examssupervision_pi1[fak_id]":View.m_faculty,
                "tx_examssupervision_pi1[action]":"getFacultyStats"
            };

            this.report_exam_count_container.appendChild(ElementHelper.createLoadingIcon());
            this.report_exam_no_sup_count_container.appendChild(ElementHelper.createLoadingIcon());

            SupervisionAjax.get(this.AjaxData, this.setStats.bind(this));
        }

        setStats(Answer){
            if(Answer.Result === SupervisionAjax.Result.Success){
                this.report_exam_count_container.innerHTML = Answer.Data.ExamsCount;
                this.report_exam_no_sup_count_container.innerHTML = Answer.Data.ExamsWithoutSupervisors;
            }else{
                SupervisionAjax.showServerErrorMessage(Answer);
            }
        }
    }
