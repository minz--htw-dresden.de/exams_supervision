import PruefungListBaseView from "./PruefungListBaseView.js";
import ServiceModuleOwnFakTableHandler from "../Handler/ServiceModuleOwnFakTableHandler.js";
import SupervisionAjax from "../SupervisionAjax.js";
import Toast from "../Utils/Toast.js";

export default class ServiceModuleOwnView extends PruefungListBaseView{

    constructor(){
        super("ownFak");
        this.viewName = "Servicemodule für die eigene Fakultät";
        this.registerActivator(document.getElementById("v-pills-nav-servicemodule-tab"));
        this.tableHandler = new ServiceModuleOwnFakTableHandler(document.getElementById(this.praefix + "_exams_list"));
        this.tableHandler.addTemplate("row", Globals.getTemplate("serviceModulOwnFakRowTemplate", 0));
        this.tableHandler.OnTableRowClick.add("rowClick", this.onTableRowClick.bind(this));
    }

    onSaveButtonClick(){
        var supervisors = this.plannedSupervisors.querySelectorAll(".exam_supervisor_list");
        var exam_id = this.tableHandler.selected.id;
        var s_arr = [];

        supervisors.forEach((element) =>{
            s_arr.push(element.getAttribute("data-id"));
        });

        var data = {
            "tx_examssupervision_pi1[exam_id]":exam_id,
            "tx_examssupervision_pi1[action]":"shareSupervisorForExam",
            "tx_examssupervision_pi1[supervisors]":JSON.stringify(s_arr)
        };
        SupervisionAjax.get(data, (data)=>{
            if(data.Result === SupervisionAjax.Result.Success){
                let toast = new Toast(Globals.getTemplate("toastDefaultTemplate", 0));
                toast.set("Speichern", "Gespeichert...");
                toast.show();
            }else{
                SupervisionAjax.showServerErrorMessage(data);
            }
        });
        this.m_isDirty = false;
    }
}