import PruefungListBaseView from "./PruefungListBaseView.js";
import ServiceModuleOtherFakTableHandler from "../Handler/ServiceModuleOtherFakTableHandler.js";
import Toast from "../Utils/Toast.js";
import SupervisionAjax from "../SupervisionAjax.js";

export default class ServiceModuleOtherView extends PruefungListBaseView{

    constructor(){
        super("otherFak");
        this.viewName = "Servicemodule für andere Fakultäten";
        this.registerActivator(document.getElementById("v-pills-nav-servicemodule-tab"));
        this.tableHandler = new ServiceModuleOtherFakTableHandler(document.getElementById(this.praefix + "_exams_list"));
        this.tableHandler.addTemplate("row", Globals.getTemplate("serviceModulOtherFakRowTemplate", 0));
        this.tableHandler.OnTableRowClick.add("rowClick", this.onTableRowClick.bind(this));
    }

    onSaveButtonClick(){
        var supervisors = this.plannedSupervisors.querySelectorAll(".exam_supervisor_list");
        var exam_id = this.tableHandler.selected.id;
        var status = document.querySelector("[id=" + this.praefix + "_data-state]").checked ? 3 : 0;
        var s_arr = [];

        supervisors.forEach((element) =>{
            s_arr.push(element.getAttribute("data-id"));
        });

        var data = this.tableHandler.getAction(
        {
            exam_id: exam_id,
            supervisors: JSON.stringify(s_arr),
            action: "setPlannedSharedSupervisors",
            status: status
        });
        
        SupervisionAjax.get(data, (data)=>{
            if(data.Result === SupervisionAjax.Result.Success){
                let toast = new Toast(Globals.getTemplate("toastDefaultTemplate", 0));
                toast.set("Speichern", "Gespeichert...");
                toast.show();

                //update the exam
                let tr = this.tableHandler.table.querySelector("[data-id='" + data.Data.id + "']");
                this.tableHandler.setRowData(tr, data.Data);
                
            }else{
                SupervisionAjax.showServerErrorMessage(data);
            }
        });
        this.m_isDirty = false;
    }
}