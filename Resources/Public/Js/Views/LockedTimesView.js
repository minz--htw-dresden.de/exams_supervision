import View from "../View.js";
import Modal from "../Utils/Modal.js";
import Toast from "../Utils/Toast.js";
import InputFilterControl from "../Utils/InputFilterControl.js";
import FilterControl from "../Utils/FilterControl.js";
import SupervisionAjax from "../SupervisionAjax.js";

    export class LockedTimesView extends View{
        constructor(){
            super();
            this.lockedTimeTableHandler = null;
            this.selectedEmployee = null;
            this.onIsDirty.add("onLockedTimesDirty", this.onIsDirtyCall.bind(this));
            this.Supervisionselect = document.getElementById("supervision_sperrzeiten_input");
            this.Supervisionselect_parent = this.Supervisionselect.parentNode;
            
            var SelectGroup = InputFilterControl.createControlGroup("employee", this.Supervisionselect, null);
            this.EmployeeFilter = SelectGroup.instance;
            this.EmployeeFilter.onChange.add("employeeChange", this.onSupervisorSperrzeitenChanged.bind(this));
            this.Supervisionselect_parent.appendChild(SelectGroup.group);
            this.Supervisionselect = this.EmployeeFilter.control;
            SelectGroup.group.querySelector("button").onclick = () => SelectGroup.instance.reset();

            this.saveButton = document.getElementById("LockedTimesSaveButton");
            this.saveButton.onclick = this.onSaveSperrzeitenButtonClick.bind(this);

            this.registerActivator(document.getElementById("v-pills-nav-sperrzeiten-tab"));
        }

        onFakSperrzeitenChangeSuccess(data){
            if(data.Result === SupervisionAjax.Result.Success){
                this.EmployeeFilter.clear();
                var employees = data.Data;
                for(var i = 0; i < employees.length; i++){
                    let strings = [employees[i].last_name, employees[i].first_name];
                    if(employees[i].title.length > 0) strings.push(employees[i].title);
                    var opElem = this.EmployeeFilter.addFilterOption(strings.join(", "), "", true);
                    opElem.setAttribute("data-id", employees[i].id);
                }
                this.EmployeeFilter.sortFilterOptions(FilterControl.Functions.AlphabetSort);
                this.ChangeTable();
            }else{
                SupervisionAjax.showServerErrorMessage(data);
            }
        }

        update(){
            super.update();
            this.onFakSperrzeitenChange();
        }

        onIsDirtyCall(){
            var modal = new Modal(Globals.getTemplate("modalDefaultTemplate", 0), "LockedTimesDirtyModal");
            modal.set("Ungesicherte Änderungen", "Im View Sperrzeiten befinden sich ungesicherte Änderungen.");
            modal.addButton("Speichern & weiter", this.SaveAndChangeTable.bind(this));
            modal.addButton("Nicht Speichern & weiter", this.ChangeTable.bind(this));
            //modal.onCloseButtonClick.add("ViewModalCloseButtonClick", this.resetSupervisorSperrzeiten.bind(this));
            modal.show();
        }

        onFakSperrzeitenChange(){
            var fakID = View.m_faculty;
            var data = { 
                    "tx_examssupervision_pi1[fak_id]":fakID,
                    "tx_examssupervision_pi1[action]":"getEmployees"
                };
            SupervisionAjax.get(data, this.onFakSperrzeitenChangeSuccess.bind(this));
        }

        SaveAndChangeTable(){
            this.onSaveSperrzeitenButtonClick();
            this.ChangeTable();
        }

        ChangeTable(){
            var select = this.EmployeeFilter.control;
            var dataList = document.getElementById(this.EmployeeFilter.getDataListId());

            //get the best fitting option
            var op = dataList.querySelector("[value='" + select.value + "']");
            if(op == undefined){
                var options = Array.from(dataList.options).map(function(el){
                    return el.value;
                });
                var relevantOptions = options.filter(function(option){
                    return option.toLowerCase().includes(select.value.toLowerCase());
                }); 
                if(relevantOptions.length > 0){
                    select.value = relevantOptions.shift();
                }else{
                    select.value = options.shift();
                }
                op = dataList.querySelector("[value='" + select.value + "']");
            }
            if(op == null) return;
            var emp = op.getAttribute("data-id");

            this.lockedTimeTableHandler.setAjaxData(emp);
            this.lockedTimeTableHandler.getTableData();
            this.m_isDirty = false;
        }

        onSupervisorSperrzeitenChanged(e){
            if(this.isDirty() == false){
                this.ChangeTable();
                this.selectedEmployee = e.target.value;
            }
        }

        deleteEntryClick(event){
            var view = View.active;
            var id = event.target.closest("tr").getAttribute("data-id");
            var emp = view.EmployeeFilter.getSelectedOptionElement().getAttribute("data-id");
            var data = { 
                "tx_examssupervision_pi1[lt_id]":id,
                "tx_examssupervision_pi1[emp_id]":emp,
                "tx_examssupervision_pi1[action]":"deleteLockedTime"
            };
            Modal.ask("Eintrag löschen?", () =>{
                SupervisionAjax.get(data, (data)=>{
                    if(data.Result === SupervisionAjax.Result.Success){
                        event.target.closest("tr").remove();
                    }else{
                        SupervisionAjax.showServerErrorMessage(data);
                    }
                });
            }, () =>{});
        }

        onSaveSperrzeitenButtonClick(){
            var value = this.Supervisionselect.value;
            var emp = this.Supervisionselect.list.querySelector("[value='" + value + "']").getAttribute("data-id");

            var times = this.lockedTimeTableHandler.extractLockedTimesFromTable();
            var data = { 
                    "tx_examssupervision_pi1[emp_id]":emp,
                    "tx_examssupervision_pi1[lockedTimes]":JSON.stringify(times),
                    "tx_examssupervision_pi1[action]":"setLockedTimes"
                };
            SupervisionAjax.get(data, (data)=>{
                if(data.Result === SupervisionAjax.Result.Success){
                    var toast = new Toast(Globals.getTemplate("toastDefaultTemplate", 0));
                    toast.set("Speichern", "Gespeichert...");
                    toast.show();
                }else{
                    SupervisionAjax.showServerErrorMessage(data);
                }
            });
            this.m_isDirty = false;
        }
    }
