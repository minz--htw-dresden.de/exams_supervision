import PruefungListBaseView from "./PruefungListBaseView.js";
import { ExamsTableHandler } from "../Handler/ExamsTableHandler.js";
import SupervisionAjax from "../SupervisionAjax.js";
import Toast from "../Utils/Toast.js";

export class PruefungenView extends PruefungListBaseView{
    constructor(){
        super("own_exams");
        this.viewName = "Prüfungen";
        this.tableHandler = new ExamsTableHandler(document.getElementById(this.praefix + "_exams_list"), this.praefix);
        this.tableHandler.addTemplate("row", Globals.getTemplate("examRowTemplate", 0));
        this.tableHandler.OnTableRowClick.add("rowClick", this.onTableRowClick.bind(this));
    }

    onSaveButtonClick(){
        var supervisors = this.plannedSupervisors.querySelectorAll(".exam_supervisor_list");
        var exam_id = this.tableHandler.selected.id;
        var status = document.querySelector("[id=" + this.praefix + "_data-state]").checked ? 3 : 0;
        var s_arr = [];

        supervisors.forEach((element) =>{
            s_arr.push(element.getAttribute("data-id"));
        });

        var data = {
            "tx_examssupervision_pi1[exam_id]":exam_id,
            "tx_examssupervision_pi1[action]":"setPlannedSupervisors",
            "tx_examssupervision_pi1[supervisors]":JSON.stringify(s_arr),
            "tx_examssupervision_pi1[status]": status
        };
        SupervisionAjax.get(data, (data)=>{
            if(data.Result === SupervisionAjax.Result.Success){
                let toast = new Toast(Globals.getTemplate("toastDefaultTemplate", 0));
                toast.set("Speichern", "Gespeichert...");
                toast.show();

                //update the exam
                let tr = this.tableHandler.table.querySelector("[data-id='" + data.Data.id + "']");
                this.tableHandler.setRowData(tr, data.Data);

            }else{
                SupervisionAjax.showServerErrorMessage(data);
            }
        });
        console.log(s_arr);
        this.m_isDirty = false;
    }
}