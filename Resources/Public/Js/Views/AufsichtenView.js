import View from "../View.js";
import Toast from "../Utils/Toast.js";
import SupervisionAjax from "../SupervisionAjax.js";

    export class AufsichtenView extends View{
        constructor(){
            super();
            this.supervisionTableHandler = null;
            this.reportSupervisorSelect = document.getElementById("supervision_select");
            this.registerActivator(document.getElementById("v-pills-nav-aufsichten-tab"));
            this.onBecomeActive.add("aufsichten_report_update", this.update.bind(this));
        }

        update(){
            this.supervisionTableHandler.setAjaxData(View.m_faculty);
            this.supervisionTableHandler.getTableData();
        }

        onShowReportClick(e){
            jQuery("#v-pills-nav-berichte-tab").tab("show");
            jQuery("#nav-c5278-tab").tab("show");
            this.reportSupervisorSelect.value = e.target.closest("tr").children[1].innerHTML;
            this.reportSupervisorSelect.onchange();
        }

        onPrioritySelectChange(e){
            var that = this;
            var data_id = e.target.closest("tr").getAttribute("data-id");

            var Ajaxdata = this.supervisionTableHandler.getAction({ 
                priority:e.target.value,
                emp_id: data_id,
                action:"setSupervisorPriority"
            });
            SupervisionAjax.get(Ajaxdata, (Answer) =>{
                if(Answer.Result === SupervisionAjax.Result.Success){
                    var toast = new Toast(Globals.getTemplate("toastDefaultTemplate"));
                    toast.set("...", "Priorität geändert!");
                    toast.show();
                    //TODO: Geht das besser?
                    that.supervisionTableHandler.data.forEach((elem) =>{
                        if(elem.ID == data_id) elem.Prioritaet = e.target.value;
                    });
                }else{
                    SupervisionAjax.showServerErrorMessage(Answer);
                }
            });
        }
    }
