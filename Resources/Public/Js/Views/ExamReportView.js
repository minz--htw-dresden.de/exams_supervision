import SupervisionAjax from "../SupervisionAjax.js";
import ElementHelper from "../Utils/ElementHelper.js";
import Toast from "../Utils/Toast.js";
import View from "../View.js";

    export class ExamReportView extends View{
        constructor(){
            super();
            this.examsreportTablehandler = null;
            this.selectedExam = 0;
            this.infoTable = null;
        }

        update(){
            this.examsreportTablehandler.setAjaxData(View.m_faculty);
            this.examsreportTablehandler.getTableData();
        }

        onExamReportTableRowClick(event){
            if(this.examsreportTablehandler.selected != null && this.examsreportTablehandler.selected.nextElementSibling != null){
                this.examsreportTablehandler.selected.nextElementSibling.remove();
            }
            var tr = event.target.closest("tr");
            var contTr = document.createElement("tr");
            var td = document.createElement("td");
            td.setAttribute("colspan", this.examsreportTablehandler.getHeaderCellCount());
            contTr.appendChild(td);
            tr.after(contTr);
            this.infoTable = Globals.getTemplate("ExamReportInfoTemplate", 0);
            td.appendChild(this.infoTable);
            this.examsreportTablehandler.selected = tr;
            this.selectedExam = tr.getAttribute("data-id");

            var data2 = {
                "tx_examssupervision_pi1[exam_id]":this.selectedExam,
                "tx_examssupervision_pi1[action]":"getPlannedSupervisors"
            }
            SupervisionAjax.get(data2, this.onGetSupervisorsDone.bind(this));
        }

        onGetSupervisorsDone(result){

            function createTD(inner){
                var td = document.createElement("td");
                td.innerHTML = inner;
                return td;
            }

            if(result.Result == SupervisionAjax.Result.Success){
                if(result.Data.length == 0){
                    var tr = document.createElement("tr");
                    tr.appendChild(createTD(ElementHelper.createDarkAlert("Keine Daten zum Anzeigen!").outerHTML));
                    tr.childNodes[0].setAttribute("colSpan", "4");
                    this.infoTable.querySelector("tbody").appendChild(tr);
                }else{
                    result.Data.forEach(element => {
                        var tr = document.createElement("tr");
                        tr.appendChild(createTD(element.title));
                        tr.appendChild(createTD(element.last_name));
                        tr.appendChild(createTD(element.first_name));
                        tr.appendChild(createTD(element.email));
                        this.infoTable.querySelector("tbody").appendChild(tr);
                    });
                }
            }else{
                SupervisionAjax.showServerErrorMessage(result);
            }
        }

        onExamReportToPDFButtonClick(event){
            var data = {
                "tx_examssupervision_pi1[action]":"createPDF",
                "tx_examssupervision_pi1[controller]":"PDFAPI",
                "tx_examssupervision_pi1[fakid]":  View.m_faculty,
                "tx_examssupervision_pi1[examid]":  this.selectedExam,
                "tx_examssupervision_pi1[type]":   types[2]
            };

            SupervisionAjax.getPDF(data, (Answer) =>{
                window.open(Answer.Data);
            });
        }

        onExamReportToEmailButtonClick(event){
            var data = {
                "tx_examssupervision_pi1[action]":"sendMail",
                "tx_examssupervision_pi1[controller]":"Mail",
                "tx_examssupervision_pi1[fakid]":  View.m_faculty,
                "tx_examssupervision_pi1[examid]":  this.selectedExam,
                "tx_examssupervision_pi1[type]":   types[2]
            };

            SupervisionAjax.mail(data, (Answer) =>{
                if(Answer.Result == SupervisionAjax.Result.Success){
                    let toast = new Toast(Globals.getTemplate("toastDefaultTemplate", 0));
                    toast.setTitle("Gesendet...");
                    toast.setBody("Email wurde gesendet.");
                    toast.show();
                }else SupervisionAjax.showServerErrorMessage(Answer);
            });
        }
    }
