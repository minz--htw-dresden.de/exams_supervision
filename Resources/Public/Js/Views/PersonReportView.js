//Plugins.require(Globals.JsPath + "/View.js");
import View from "./../View.js";
import SupervisionAjax from "../SupervisionAjax.js";
import InputFilterControl from "../Utils/InputFilterControl.js";
import FilterControl from "../Utils/FilterControl.js";

    export default class PersonReportView extends View{
        constructor(){
            super();
            this.selectedSupervisor = null;
            this.supervisionsTableHandler = null;

            this.Supervisionselect = document.getElementById("supervision_select");
            this.Supervisionselect_parent = this.Supervisionselect.parentNode;
            var SelectGroup = InputFilterControl.createControlGroup("employee", this.Supervisionselect, null);
            this.EmployeeFilter = SelectGroup.instance;
            this.EmployeeFilter.onChange.add("employeeChange", this.onSupervisorChanged.bind(this));
            this.Supervisionselect_parent.appendChild(SelectGroup.group);
            this.Supervisionselect = this.EmployeeFilter.control;
            SelectGroup.group.querySelector("button").onclick = () => SelectGroup.instance.reset();
        }
        
        onTabClick(e){
            tabs.forEach((element) => {
                var elem = document.getElementById(element);
                if(elem.id == e) elem.style.display = "block";
                else elem.style.display = "none";
            });
        }

        update(){
            super.update();
            this.onFakChange();
        }

        onFakChange(){
            var fakID = View.m_faculty;
            var data = { 
                    "tx_examssupervision_pi1[fak_id]":fakID,
                    "tx_examssupervision_pi1[action]":"getEmployees"
                };
            SupervisionAjax.get(data, this.onFakChangeSuccess.bind(this));
        }

        onFakChangeSuccess(data){
            if(data.Result === SupervisionAjax.Result.Success){
                this.EmployeeFilter.reset();

                data.Data.forEach((elem)=>{
                    var strings = [elem.last_name, elem.first_name];
                    if(elem.title != "") strings.push(elem.title);
                    var op = this.EmployeeFilter.addFilterOption(strings.join(", "));
                    if(op != null) op.setAttribute("data-id", elem.id);
                });
                this.EmployeeFilter.sortFilterOptions(FilterControl.Functions.AlphabetSort);
                this.onSupervisorChanged();
            }else{
                SupervisionAjax.showServerErrorMessage(data);
            }
        }

        onSupervisorChanged(){
            this.selectedSupervisor = this.EmployeeFilter.getBestFittingOption().getAttribute("data-id");
            this.supervisionsTableHandler.setAjaxData(this.selectedSupervisor);
            this.supervisionsTableHandler.getTableData();
        }

        onCreatePDFClick(type){
            var data = {
                "tx_examssupervision_pi1[action]":"createPDF",
                "tx_examssupervision_pi1[controller]":"PDFAPI",
                "tx_examssupervision_pi1[fakid]":  View.m_faculty,
                "tx_examssupervision_pi1[empid]":  this.selectedSupervisor,
                "tx_examssupervision_pi1[type]":   type
            }

            SupervisionAjax.getPDF(data, (Answer) =>{
                window.open(Answer.Data);
            });
        }

        onSendEmailClick(type){
            var data = {
                "tx_examssupervision_pi1[action]":"sendEmail",
                "tx_examssupervision_pi1[controller]":"Mail",
                "tx_examssupervision_pi1[fakid]":  View.m_faculty,
                "tx_examssupervision_pi1[empid]":  this.selectedSupervisor,
                "tx_examssupervision_pi1[type]":   type
            }
            SupervisionAjax.mail(data, (Answer) =>{
                console.log(Answer.Data);
            });
        }
    }
