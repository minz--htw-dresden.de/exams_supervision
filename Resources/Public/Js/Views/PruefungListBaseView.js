import View from "../View.js";
import InputFilterControl from "../Utils/InputFilterControl.js";
import SupervisionAjax from "../SupervisionAjax.js";
import FilterControl from "../Utils/FilterControl.js";
import { OrgEnum } from "../Utils/DataEnums.js";
import Modal from "../Utils/Modal.js";

export default class PruefungListBaseView extends View{
    constructor(praefix){
        super();
        this.praefix = praefix;
        this.tableHandler = null;
        this.viewName = this.praefix;

        this.availSupervisors = document.getElementById(this.praefix + "_exams_avail_supervisor_list");
        this.plannedSupervisors = document.getElementById(this.praefix + "_exams_planned_supervisor_list");
        this.infoContainer = document.getElementById(this.praefix + "_exam_info_container");
        this.examsInfo = document.getElementById(this.praefix + "_exam_info");

        this.availSupervisors.ondrop = this.onExamSupervisorDivDropAvail.bind(this);
        this.availSupervisors.ondragover = this.allowDropSupervisor.bind(this);
        this.plannedSupervisors.ondrop = this.onExamSupervisorDivDropPlanned.bind(this);
        this.plannedSupervisors.ondragover = this.allowDropSupervisor.bind(this);

        this.addSupervisorButton = document.getElementById(this.praefix + "_AddSupervisorButton");
        this.addSupervisorButton.onclick = this.onAddSupervisorButtonClick.bind(this);

        this.removeSupervisorButton = document.getElementById(this.praefix + "_RemoveSupervisorButton");
        this.removeSupervisorButton.onclick = this.onRemoveSupervisorButtonClick.bind(this);

        this.saveButton = document.getElementById(this.praefix + "_save_button");
        this.saveButton.onclick = this.onSaveButtonClick.bind(this);

        this.draggedItem = null;
        this.selectedItem = null;
        this.requestedItem = null;

        this.availSupervisorFilter = new InputFilterControl("Name", document.getElementById(this.praefix + "_avail_supervisor_list_filter"), null);
        this.availSupervisorFilter.onChange.add("availFilterChange", this.onAvailSupervisorsSelectFilterChange.bind(this));

        this.availSupervisorFilterClear = document.getElementById(this.praefix + "_avail_supervisor_filter_clear");
        this.availSupervisorFilterClear.onclick = this.onavailSupervisorFilterClearClick.bind(this);

        this.dataActionString1 = "getAvailableSupervisors";
        this.dataActionString2 = "getPlannedSupervisors";

        this.onIsDirty.add(this.ViewID + "_Dirty", this.onIsDirtyCall.bind(this));
    }

    onIsDirtyCall(){
        var modal = new Modal(Globals.getTemplate("modalDefaultTemplate", 0), "PruefungenDirtyModal");
        modal.set("Ungesicherte Änderungen", "Im View " + this.viewName + " befinden sich ungesicherte Änderungen.");
        modal.addButton("Speichern & weiter", this.saveAndChange.bind(this));
        modal.addButton("Nicht Speichern & weiter", this.change.bind(this));
        modal.onCloseButtonClick.add("ViewModalCloseButtonClick", this.reset.bind(this));
        modal.show();
    }

    saveAndChange(){
        this.onSaveButtonClick();
        this.change();
    }

    change(){
        this.m_isDirty = false;
        this.onTableRowClick({target:this.requestedItem});
    }

    reset(){
        this.requestedItem = this.selectedItem;
    }

    update(){
        super.update();
        this.tableHandler.setAjaxData(View.m_faculty);
        this.tableHandler.getTableData();
    }

    createActionObject(exam, fak, action){
        return {exam_id: exam, action: action, fak_id: fak};
    }

    onTableRowClick(e){
        this.requestedItem = e.target.closest("tr");
        if(this.isDirty() == false){
            this.tableHandler.table.closest(".exams_table").classList.add("exams_table_selected");
            jQuery(this.infoContainer).show('slow');

            var tr = e.target.closest("tr");
            tr.classList.add("exams_table_row_selected");

            var exam_id = tr.getAttribute("data-id");

            var data = this.tableHandler.getAction(this.createActionObject(exam_id, View.m_faculty, this.dataActionString1)); 
            var data2 = this.tableHandler.getAction(this.createActionObject(exam_id, View.m_faculty, this.dataActionString2));
            
            SupervisionAjax.get(data, this.setAvailableSupervisors.bind(this));
            SupervisionAjax.get(data2, this.setPlannedSupervisors.bind(this));
            
            if(this.tableHandler.selected != null){
                var elem = this.tableHandler.table.querySelector("[data-id='" + this.tableHandler.selected.id + "'");
                if(elem != null){
                    elem.classList.remove("exams_table_row_selected");
                }
            }
                
            this.setExamInfo(exam_id);
        }
    }

    setAvailableSupervisors(data){
        if(data.Result === SupervisionAjax.Result.Success){
            this.availSupervisors.replaceChildren();
            data.Data.forEach(element => {
                var entry = Globals.getTemplate("examSupervisorEntryTemplate", 0);
                entry.querySelector("input").id = this.praefix + "_radio_" + element.id;
                entry.querySelector("input").setAttribute("name", "avail");
                entry.onclick = this.onSupervisorListItemClick.bind(this);
                entry.querySelector("li").ondragstart = this.onExamSupervisorDivDrag.bind(this);

                let strings = [element.last_name, element.first_name];
                if(element.title != "") strings.push(element.title);
                if(element.faculty != View.m_faculty) strings.push("<span style='color: red;'>(" + OrgEnum.IdToShortStr(element.faculty) + ")</span>");

                entry.querySelectorAll("label")[0].innerHTML = strings.join(", ");
                entry.querySelectorAll("label")[1].setAttribute("for", this.praefix + "_radio_" + element.id);
                entry.setAttribute("data-id", element.id);
                if(element.faculty != View.m_faculty){
                    this.availSupervisors.prepend(entry);
                }
                else
                    this.availSupervisors.appendChild(entry);
                entry.classList.add("show");

                var option = this.availSupervisorFilter.addFilterOption(element.last_name + ", " + element.first_name);
                if(option != null)
                    option.setAttribute("data-id", element.id);

            });
        }else{
            SupervisionAjax.showServerErrorMessage(data);
        }
    }

    setPlannedSupervisors(data){
        if(data.Result === SupervisionAjax.Result.Success){
            this.plannedSupervisors.replaceChildren();
            data.Data.forEach(element => {
                var entry = Globals.getTemplate("examSupervisorEntryTemplate", 0);
                entry.querySelector("input").id = this.praefix + "_radio_" + element.id;
                entry.querySelector("input").setAttribute("name", "avail");
                entry.onclick = this.onSupervisorListItemClick.bind(this);
                entry.querySelector("li").ondragstart = this.onExamSupervisorDivDrag.bind(this);

                let strings = [element.last_name, element.first_name];
                if(element.title != "") strings.push(element.title);
                if(element.faculty != View.m_faculty) strings.push("<span style='color: red;'>(" + OrgEnum.IdToShortStr(element.faculty) + ")</span>");
                
                entry.querySelectorAll("label")[0].innerHTML = strings.join(", ");
                entry.querySelectorAll("label")[1].setAttribute("for", this.praefix + "_radio_" + element.id);
                entry.setAttribute("data-id", element.id);
                entry.classList.add("show");
                this.plannedSupervisors.appendChild(entry);
            });
        }else{
            SupervisionAjax.showServerErrorMessage(data);
        }
    }

    allowDropSupervisor(event){
        var div = this.draggedItem.parentNode.closest("div");
        if(event.currentTarget != div)
            event.preventDefault();
    }

    onExamSupervisorDivDrag(e){
        this.draggedItem = e.target.closest("div");
    }

    onExamSupervisorDivDropPlanned(e){
        this.availSupervisors.removeChild(this.draggedItem);
        this.plannedSupervisors.appendChild(this.draggedItem);
        if(this.availSupervisors.querySelectorAll(".show").length == 0)
            this.onavailSupervisorFilterClearClick();
        this.setDirty();
    }

    onExamSupervisorDivDropAvail(e){
        this.plannedSupervisors.removeChild(this.draggedItem);
        this.availSupervisors.appendChild(this.draggedItem);
        this.setDirty();
    }

    setExamInfo(id){
        var exam = this.tableHandler.data.find(element => element.id == id);
        if(exam != null)
            this.tableHandler.selected = exam;
        else{
            exam = {datum: "--", dauer: "--", art: "--", status: 0};
        }
        this.examsInfo.querySelector("[id=" + this.praefix + "_data-date]").innerHTML = exam.datum;
        this.examsInfo.querySelector("[id=" + this.praefix + "_data-length]").innerHTML = exam.dauer;
        this.examsInfo.querySelector("[id=" + this.praefix + "_data-type]").innerHTML = exam.art;
        var checkbox = document.querySelector("[id=" + this.praefix + "_data-state]");
        if(checkbox != null)
            checkbox.checked = exam.status == 3;
    }

    onAddSupervisorButtonClick(event){
        if(this.selectedItem == null) return;
        this.draggedItem = this.selectedItem;
        this.onExamSupervisorDivDropPlanned();
        this.onSupervisorListItemClick({target:this.selectedItem});
    }

    onRemoveSupervisorButtonClick(event){
        if(this.selectedItem == null) return;
        this.draggedItem = this.selectedItem;
        this.onExamSupervisorDivDropAvail();
        this.onSupervisorListItemClick({target:this.selectedItem});
    }

    onAvailSupervisorsSelectFilterChange(event){
        var selected = this.availSupervisorFilter.getSelectedOptionElement();
        var inputStr = this.availSupervisorFilter.control.value;
        var divs = this.availSupervisors.querySelectorAll("div");

        if(selected == null){
            divs.forEach((elem) => {
                if(FilterControl.Functions.IndexCompare(elem.querySelector("label").innerHTML, inputStr.toLowerCase()))
                    elem.classList.add("show");
                else
                    elem.classList.remove("show");
            });
            return;
        }

        divs.forEach((element) => {
            if(element.getAttribute("data-id") != selected.getAttribute("data-id")){
                element.classList.remove("show");
            }else{
                element.classList.add("show");
            }
        })
    }

    onSupervisorListItemClick(event){
        this.selectedItem = event.target.closest("div");
        if(this.selectedItem.parentNode.closest("div") == this.availSupervisors){
            this.addSupervisorButton.removeAttribute("disabled");
            this.removeSupervisorButton.setAttribute("disabled", true);
        }else{
            this.removeSupervisorButton.removeAttribute("disabled");
            this.addSupervisorButton.setAttribute("disabled", true);
        }
    }

    onavailSupervisorFilterClearClick(event){
        this.availSupervisorFilter.reset();
        this.onAvailSupervisorsSelectFilterChange();
    }

    onSaveButtonClick(){}

    resetView(){
        this.availSupervisors.replaceChildren();
        this.plannedSupervisors.replaceChildren();
        this.draggedItem = null;
        this.selectedItem = null;
        this.requestedItem = null;
        this.tableHandler.setDataSets([]);
        this.tableHandler.setRows();
        this.setExamInfo(-1);
    }

    onKeyDown(event){
        switch(event.code){
            case "Delete":
                this.onRemoveSupervisorButtonClick();
        }
    }
}