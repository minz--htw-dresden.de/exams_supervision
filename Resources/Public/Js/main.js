
import { AufsichtenView } from "./Views/AufsichtenView.js";
import { FacultyReportView } from "./Views/FacultyReportView.js";
import PersonReportView from "./Views/PersonReportView.js";
import { ExamReportView } from "./Views/ExamReportView.js";
import { PruefungenView } from "./Views/PruefungenView.js";
import { LockedTimesView } from "./Views/LockedTimesView.js";
import ServiceModuleOwnView from "./Views/ServiceModulOwnView.js";
import ServiceModuleOtherView from "./Views/ServiceModulOtherView.js";

import SupervisorTableHandler from "./Handler/SupervisorTableHandler.js";
import { SupervisionsTableHandler } from "./Handler/SupervisionsTableHandler.js";
import { ExamsReportTableHandler } from "./Handler/ExamsReportTableHandler.js";
import { ExamsTableHandler } from "./Handler/ExamsTableHandler.js";
import { LockedTimesTableHandler } from "./Handler/LockedTimesTableHandler.js";
import View from "./View.js";
import Templates from "./Utils/Templates.js";
import SupervisionAjax from "./SupervisionAjax.js";

Globals.Templates = new Templates();

/**
 * Get the First Faculty we are allowed to aperate on.
 */
try{
    var id = document.getElementById("v-pills-tab-fak-dropdown").querySelector(".dropdown-menu").querySelector("a").getAttribute("data-id");
    View.setFacultyGlobal(id);
}catch(e){
    SupervisionAjax.showServerErrorMessage({Error:{textStatus: "Keine zur Bearbeitung freigegebenen Fakultäten gefunden!", errorThrown: "", Status: 403}});
    throw e;
}

//
if(document.querySelector("#servicenav").getAttribute("style") == null){
    setTimeout(()=>{
        let div = document.querySelector("#full-screen-option");
        div.click();
    }, 500);
}

var PaufsichtView = new AufsichtenView();
PaufsichtView.supervisionTableHandler = new SupervisorTableHandler(
                            document.getElementById("supervisorsTable"),
                            document.getElementById("supervisor_pagination"));
PaufsichtView.supervisionTableHandler.addTemplate("row", Globals.getTemplate("supervisionStatRowEntryTemplate", 0));

var PersonReport = new PersonReportView();
PersonReport.supervisionsTableHandler = new SupervisionsTableHandler(document.getElementById("planned_Exams_Table"));
PersonReport.supervisionsTableHandler.addTemplate("row", Globals.getTemplate("examPlannedSupervisionTemplate", 0));

var ExamReport = new ExamReportView();
ExamReport.examsreportTablehandler = new ExamsReportTableHandler(document.getElementById("fak_Exams_Table"), 
                                                                        document.getElementById("exam_report_pagination"));
ExamReport.examsreportTablehandler.addTemplate("row", Globals.getTemplate("examReportEntryTemplate", 0));

new FacultyReportView();

var ServiceModule = new ServiceModuleOtherView();
new ServiceModuleOwnView();

var PPruefungView = new PruefungenView();
/*
PPruefungView.examsTableHandler = new ExamsTableHandler(document.getElementById("exams_list"));
PPruefungView.examsTableHandler.addTemplate("row", Globals.getTemplate("examRowTemplate", 0));
PPruefungView.examsTableHandler.examsInfo = document.getElementById("exam_info");
*/

var LTView = new LockedTimesView();
var tableElement = document.getElementById("lockedTimesTable");
LTView.lockedTimeTableHandler = new LockedTimesTableHandler(tableElement);
LTView.lockedTimeTableHandler.addTemplate("row", Globals.getTemplate("lockedTimeTableRowTemplate", 0));
LTView.lockedTimeTableHandler.OnChanged.add("setLockedTimesViewDirty", LTView.setDirty.bind(LTView));


View.updateGlobal();

//Write the views into Global Namespace
window.PPruefungView = PPruefungView;
window.ExamReport = ExamReport;
window.LTView = LTView;
window.PersonReport = PersonReport;
window.PaufsichtView = PaufsichtView;
window.ServiceModuleView = ServiceModule;


/**
 * Global Event Listeners
 */

/**
 * View: Main
 * Element: Dropdown
 * Selector: Faculty
 */
 document.querySelector("#v-pills-tab-fak-dropdown").querySelectorAll("a").forEach(element => {
    jQuery(element).on("click", (event) => {
        if(event.target.getAttribute("data-id") == null) return;

        event.target.closest("ul").querySelector("span[selection-label]").innerHTML = event.target.innerHTML;

        View.setFacultyGlobal(event.target.getAttribute("data-id"));
        View.resetViewGlobal();
        View.updateGlobal();
    });
});

/**
 * View: PersonReport
 * Element: Select
 * Selector: Supervisor
 */

jQuery(document.getElementById("supervision_select")).on("change", (event) => {
    PersonReport.onSupervisorChanged();
});

/**
 * View: PersonReport
 * Element: Button
 * Action: Create PDF
 */

jQuery(document.getElementById("personReportCreatePDFButton")).on("click", (event)=> {
    PersonReport.onCreatePDFClick(types[1]);
});

/**
 * View: PersonReport
 * Element: Button
 * Action: Send Email
 */

 jQuery(document.getElementById("personReportSendEmailButton")).on("click", (event)=> {
    PersonReport.onSendEmailClick(types[1]);
});

/**
 * View: LockedTimes
 * Element: Button
 * Action: Save
 */

 jQuery(document.getElementById("lockedTimesSaveButton")).on("click", (event)=> {
    LTView.onSaveSperrzeitenButtonClick();
});

/**
 * View: Base
 * Element: Keyboard
 * Action: OnKeyDown
 */

 document.body.addEventListener("keydown", View.onKeyDown);