<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'AppsTeam.ExamsSupervision',
    'Pi1',
    'Supervision Planner for Exams',
    'EXT:exams_supervision\Resources\Public\Icons\Extension.svg'
);