<?php

defined("TYPO3_MODE") or die();

call_user_func(function(){
    $extensionsKey = "exams_supervision";

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionsKey,
        "Configuration/TypoScript",
        "Exams_Supervision"
    );
});
