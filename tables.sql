-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 26. Jan 2022 um 14:36
-- Server-Version: 5.7.36
-- PHP-Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Datenbank: `apps`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `exams_supervision_supervisor`
--

CREATE TABLE IF NOT EXISTS `exams_supervision_supervisor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `m_pers_id` int(11) NOT NULL,
  `exams_planner_pruefung_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `m_pers_id` (`m_pers_id`,`exams_planner_pruefung_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `exams_supervision_pers_locked`
--

CREATE TABLE IF NOT EXISTS `exams_supervision_pers_locked` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `m_pers_id` int(11) NOT NULL,
  `m_date_from` date NOT NULL,
  `m_date_to` date NOT NULL,
  `m_time_from` time DEFAULT NULL,
  `m_time_to` time DEFAULT NULL,
  `m_all_day` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `exams_supervision_pers_info`
--

CREATE TABLE IF NOT EXISTS `exams_supervision_pers_info` (
  `m_pers_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `exams_supervision_access`
--

CREATE TABLE IF NOT EXISTS `exams_supervision_access` (
  `typo3_uid` int(11) NOT NULL,
  `fk_orgeinheit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabellenstruktur für Tabelle `exams_supervision_exam_info`
--

CREATE TABLE `exams_supervision_exam_info` (
  `fk_exams_planner_pruefung_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

COMMIT;