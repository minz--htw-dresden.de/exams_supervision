lib.pageId = TEXT
lib.pageId.data = TSFE:id

plugin.tx_examssupervision_pi1 {
  settings {
    bypass_user_access_for_faculties = false
    }
}

ajax = PAGE
ajax {
  typeNum = 972514854554
  config {
    disableAllHeaderCode = 1
    additionalHeaders = Content-type:application/json
    admPanel = 0
    debug = 0
  }
  10 = USER_INT
   10 {
      userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
      extensionName = ExamsSupervision
      pluginName = Pi1
      vendorName = AppsTeam
      controller = JSONAPI
      switchableControllerActions {
         JSONAPI {
            1 = getEmployees
            2 = getLockedTimes
            3 = setLockedTimes
            4 = getExamsForFaculty
            5 = getAvailableSupervisors
            6 = setPlannedSupervisors
            7 = getPlannedSupervisors
            8 = getSupervisorStats
            9 = getPlannedSupervisionsForEmployee
           10 = setSupervisorPriority
           11 = getFacultyStats
         }
     }
   }
}

pdf < ajax
pdf {
  typeNum = 972514854555
  config {
    disableAllHeaderCode = 1
    additionalHeaders = Content-Type: application/pdf
    admPanel = 0
    debug = 0
  }
  10 = USER_INT
   10 {
      userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
      extensionName = ExamsSupervision
      pluginName = Pi1
      vendorName = AppsTeam
      controller = JSONAPI
      switchableControllerActions {
         PDFAPI {
            1 = createPDF
            2 = sendEmail
         }
     }
   }
}

mail < ajax
mail {
  typeNum = 972514854556
  config {
    disableAllHeaderCode = 1
    additionalHeaders = Content-Type: text/plain
    admPanel = 0
    debug = 0
  }
  10 = USER_INT
   10 {
      userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
      extensionName = ExamsSupervision
      pluginName = Pi1
      vendorName = AppsTeam
      controller = Mail
      switchableControllerActions {
         Mail {
            1 = sendEmail
         }
     }
   }
}
  
