<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'typo3-cms-extension',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '962c1ac753443550a8442754027fde6de57bc99f',
        'name' => 'apps/typo3-cms-ext-exams-supervision',
        'dev' => true,
    ),
    'versions' => array(
        'apps/typo3-cms-ext-exams-supervision' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'typo3-cms-extension',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '962c1ac753443550a8442754027fde6de57bc99f',
            'dev_requirement' => false,
        ),
    ),
);
