#!/bin/bash

cd /var/www/html/apps/
sudo chown -R deploy:deploy web/
sudo chown -R deploy:deploy packages/
composer update apps/typo3-cms-ext-exams-supervision --ignore-platform-reqs
sudo chown -R www-data:www-data web/
sudo chown -R www-data:www-data packages/
