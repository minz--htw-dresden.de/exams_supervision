<?php

if(!defined('TYPO3_MODE')){
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'AppsTeam.ExamsSupervision',
    'Pi1',
    [
        'SupervisionMain' => 'index',
        'JSONAPI' => 'getEmployees,
                        getLockedTimes,
                        setLockedTimes,
                        getExamsForFaculty,
                        getAvailableSupervisors,
                        setPlannedSupervisors,
                        getPlannedSupervisors,
                        getSupervisorStats,
                        getPlannedSupervisionsForEmployee,
                        setSupervisorPriority,
                        getFacultyStats,
                        deleteLockedTime,
                        getOwnServiceModulesForFaculty,
                        getOtherServiceModulesForFaculty,
                        shareSupervisorForExam,
                        setPlannedSharedSupervisors',
        'PDFAPI' => 'createPDF,
                        sendEmail',
        'Mail' => 'sendEmail'
    
    ],
    [
        'SupervisionMain' => '',
        
    ]
);
